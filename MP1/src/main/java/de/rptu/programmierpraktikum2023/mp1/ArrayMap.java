package de.rptu.programmierpraktikum2023.mp1;
public class ArrayMap implements Map<String, Integer> {

    MyMap e = new MyMap(null, null);

    // the array paare to store the entries
    public MyMap[] paare = { e };
    private int size = 0;

    @Override
    public Integer get(String key) {
        Integer result = null;
        for (MyMap map1 : paare) {
            // return the value of an entry if his key equals keys or null if such a key doesn't exist
            if (map1 != null && map1.key == key) {
                    result = map1.value;
            }
        }
        return result;
    }

    @Override
    public void put(String key, Integer value) {

        // if size is 0 we just create a new pair and add it to the elements

        if (this.size == 0) {
            this.paare[0] = new MyMap(key, value);
            this.size++;
        } else {

            // we update the value of the key if it already exists in the array

            boolean updated = false;
            for (MyMap map2: paare) {
                if (map2 != null && map2.key == key) {
                        map2.value = value;
                        updated = true;
                }
            }
            if (!updated) {

                //we create a temporary array to store all the entries

                int l = paare.length + 1;
                MyMap[] newPaare = new MyMap[l];
                int i = 0;
                for (MyMap map3: paare ) {
                    newPaare[i] = map3;
                    i++;
                }
                newPaare[i] = new MyMap(key, value);

                // now all entries are restore in the array paare

                this.paare = newPaare;
                this.size++;
            }
        }
    }

    @Override
    public void remove(String key) {

        //  check if the keys exists in the entries and set his value to null and reduce the length of entries

        for (MyMap map4: paare) {
            if (map4 != null && map4.key == key) {
                map4.value = null;
                this.size--;
            }
        }
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public void keys(String[] array) throws IllegalArgumentException {

        //first check if the array ist not null and have enough place to store the entries

        if (array == null || array.length < this.size) {
            throw new IllegalArgumentException();
        } else {

            // if there is enough place we copy all keys those values are not null in the array

            int i = 0;
            for (MyMap map5 : this.paare) {
                if (map5 != null && map5.value != null) {
                    array[i] = map5.key;
                    i++;
                }
            }
        }
    }
}
