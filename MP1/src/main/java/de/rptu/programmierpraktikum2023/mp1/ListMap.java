package de.rptu.programmierpraktikum2023.mp1;

public class ListMap<K,V> implements Map<K,V> {

    private Pair<K,V> head = new Pair<>(null, null, null);
    private int size = 0;


    @Override
    public V get(K key) {
         Pair<K,V> currentPair = this.head;
         while (currentPair != null){
             if (currentPair.key == key){
                 return currentPair.value;
             }
             currentPair = currentPair.next;
         }
             return null;//the key does not exist
    }


    @Override
        public void put(K key, V value) {
            Pair<K, V> currentPair = this.head;

            while (currentPair != null) {

                // we update the value if the key already exist

                if (currentPair.key == key) {
                    currentPair.value = value;
                    return;
                }
                currentPair = currentPair.next;
            }

            Pair<K, V> newPair = new Pair<>(key, value, null);

            if (size == 0) {

                // if the size of the list is 0 we just update it and increase the size

                this.head = newPair;
            } else {
                currentPair = this.head;
                while (currentPair.next != null) {
                    currentPair = currentPair.next;
                }
                currentPair.next = newPair;
            }

            this.size++;
        }

    @Override
    public void remove(K key) {
        Pair<K,V> currentPair = this.head;

        //change the value of the corresponding key to null and reduce the size

        while (currentPair != null) {
            if (currentPair.key == key) {
                currentPair.value = null;
                this.size--;
            }
            currentPair = currentPair.next;}
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public void keys(K[] array) {
        if (array == null || array.length < this.size) {
            throw new IllegalArgumentException();
        } else {
            Pair<K, V> currentPair = this.head;
            int index = 0;

            //pass through the list to copy the keys and insert them in the array

            while (currentPair != null && index < this.size) {
                if (currentPair.value != null) {
                    array[index] = currentPair.key;
                    index++;
                }
                currentPair = currentPair.next;
            }
        }
    }
}
