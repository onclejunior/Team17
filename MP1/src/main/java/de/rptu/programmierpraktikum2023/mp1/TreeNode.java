package de.rptu.programmierpraktikum2023.mp1;

public class TreeNode<K,V> {
    private final K key;
    private V value;
    private TreeNode<K,V> rightChild;
    private TreeNode<K,V> leftChild;

    public TreeNode(K nodeKey, V nodeValue, TreeNode<K,V> leftSuccessor, TreeNode<K,V> rightSuccessor){
        key = nodeKey;
        value = nodeValue;
        rightChild = rightSuccessor;
        leftChild= leftSuccessor;
    }
    public K getKey() {
        return this.key;
    }
    public V getValue(){
        return this.value;
    }
    public void setValue(V nodeValue){
        this.value = nodeValue;
    }
    public TreeNode<K,V> getLeftChild(){
        return this.leftChild;
    }
    public TreeNode<K,V> getRightChild() {
        return this.rightChild;
    }
    public void setLeftChild(TreeNode<K,V> newNode) {
        this.leftChild = newNode;
    }
    public void setRightChild(TreeNode<K,V> newNode) {
        this.rightChild = newNode;
    }



}
