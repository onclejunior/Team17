package de.rptu.programmierpraktikum2023.mp1;
import java.util.Comparator;

public class TreeMap<K, V> implements Map<K, V> {
    TreeNode<K, V> treeRoot;
    private int size;
    private final Comparator<K> comparator;
    public TreeMap(Comparator<K> comparator) {
        this.comparator = comparator;
    }

    @Override
    public V get(K key) {
        TreeNode<K, V> node = treeRoot;
        while (node != null) {
            //vergleicht key mit einem knoten, um richtigen teilbaum auszuwählen
            int comparisonResult = comparator.compare(key, node.getKey());
            if (comparisonResult == 0) {
                //wenn node mit key gefunden
                return node.getValue();
            } else if (comparisonResult < 0) {
                node = node.getLeftChild();
            } else {
                node = node.getRightChild();
            }
        }
        return null;
    }


    @Override
    public void put(K key, V value) {
        TreeNode<K, V> node = treeRoot;
        //wenn baum leer dann ganzer baum nur ein knoten
        if (treeRoot == null) {
            treeRoot = new TreeNode<>(key, value, null, null);
            this.size++;
            return;
        }

        while (node != null) {
            int comparisonResult = comparator.compare(key, node.getKey());
            //gleicher key, dann nur value anpassen
            if (comparisonResult == 0) {
                node.setValue(value);
                return;

            //in linken teilbaum einfügen
            } else if (comparisonResult < 0) {
                if (node.getLeftChild() == null) {
                    node.setLeftChild(new TreeNode<>(key, value, null, null));
                    this.size++;
                    return;
                } else {
                    node = node.getLeftChild();
                }

            //in rechten teilbaum einfügen
            } else {
                if (node.getRightChild() == null) {
                    node.setRightChild(new TreeNode<>(key, value, null, null));
                    this.size++;
                    return;
                } else {
                    node = node.getRightChild();
                }
            }
        }
    }

    @Override
    public void remove(K key) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public void keys(K[] array) {
        TreeNode<K, V> node = treeRoot;
        //wenn array unzulässig (leer oder zu kurz)
        if (array == null || array.length < this.size) {
            throw new IllegalArgumentException("invalid array size");
        }

        int index = 0;
        //rekursive hilfsfunktion aufrufen, um baum zu durchlaufen
        recursiveKeys(node, array, index);
    }

    private int recursiveKeys(TreeNode<K, V> node, K[] array, int index) {
        //bis der knoten keine kinder/teilbäume mehr hat
        if (node != null) {
            //jeder index der arrays bekommt einen knoten zugeteilt
            array[index] = node.getKey();
            //der index wird erhöht und ein neuer aufruf der funktion findet einen neuen knoten
            index++;
            //linker teilbaum
            index = recursiveKeys(node.getLeftChild(), array, index);
            //rechter teilbaum
            index = recursiveKeys(node.getRightChild(), array, index);
        }
        return index;
    }
}