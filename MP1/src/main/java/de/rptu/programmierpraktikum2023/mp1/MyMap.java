package de.rptu.programmierpraktikum2023.mp1;

//a class of entries that we store in the array
 public class MyMap {
    String key;
    Integer value;
    public MyMap(String key, Integer value) {
        this.key = key;
        this.value = value;
    }
}
