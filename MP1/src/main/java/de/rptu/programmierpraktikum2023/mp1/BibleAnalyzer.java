package de.rptu.programmierpraktikum2023.mp1;

public class BibleAnalyzer {
    public static void countWords(Map<String, Integer> counts) {
        for (String word : Util.getBibleWords()) {   //jedes Wort der Bibel durchgehen und Anzahl ermitteln
            Integer value = counts.get(word);
            if (value == null) {
                counts.put(word, 1);       //Wort erstes mal "gesehen" -> Anzahl auf 1
            } else {
                counts.put(word, ++value);          //jedes weitere mal wird Anzahl um 1 erhöht
            }
        }
    }

    public static void main(String[] args) {
        Map<String, Integer> map = new ListMap<String, Integer>();
        countWords(map);

        String[] array = new String[map.size()];     //Wörter in Array speichern

        sort(array, map);
        for (int i = 0; i < map.size(); i++){
            System.out.println("" + map.get(array[i]) + " " + array[i]);    //sortiertes Array wird ausgegeben

        }
    }

    public static void sort(String[] words, Map<String, Integer> counts) {
        for(int i = 0; i < counts.size(); i++) {
            String smallestWord = words[0];
            //Aktuelles Wort wird als Kleinster value angenommen
            int smallest = counts.get(words[i]);
            for (String word : words) {
                //Wenn neuer Wert kleiner als bisher, dann neuer Kleinster Wert
                if (counts.get(word) < smallest) {
                    smallest = counts.get(word);
                    smallestWord = word;
                }
            }
            if(smallest != i){
                //Temporaeres array, um Wort an richtige Position zu setzen
                String temp = words[i];
                words[i] = smallestWord;
                words[smallest] = temp;
            }
        }
    }
}