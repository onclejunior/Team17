package de.rptu.programmierpraktikum2023.mp1;
//this class represents the elements of ListMap
public class Pair<K,V> {
    K key;
    V value;
    Pair<K, V> next;

    public Pair(K key, V value, Pair<K,V> next) {
        this.key = key;
        this.value = value;
        this.next = next;
    }
}
