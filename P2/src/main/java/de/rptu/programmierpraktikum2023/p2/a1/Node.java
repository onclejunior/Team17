package de.rptu.programmierpraktikum2023.p2.a1;

public class Node<D> {
    private int id;
    private D data;

    public Node(int ID, D Data){
        this.id = ID;
        this.data = Data;
    }

    public int getId() {
        return this.id;
    }
    public D getData() { return this.data; }
}
