package de.rptu.programmierpraktikum2023.p2.a2;

import de.rptu.programmierpraktikum2023.p2.a1.*;

import java.util.Set;

public class GameMoveImpl implements GameMove {
    private final Graph<Color, Integer> graph;

    public GameMoveImpl(Graph<Color, Integer> graph) {
        this.graph = graph;
    }

    @Override
    public void setColor(int nodeId, Color color) throws GraphException, ForcedColorException {
        try {
            Color currentNodeColor = graph.getData(nodeId);
            if (currentNodeColor == color) {
                return;
            }

            graph.setData(nodeId, color);
            checkColorChange(nodeId);
        } catch (InvalidNodeException e) {
            throw new GraphException("Invalid node: " + nodeId);
        }
    }

    @Override
    public void increaseWeight(int fromId, int toId) throws GraphException {
        try {
            Integer currentWeight = graph.getWeight(fromId, toId);
            graph.setWeight(fromId, toId, currentWeight + 1);
            checkColorChange(toId);
        } catch (InvalidEdgeException e) {
          throw new GraphException("Invalid edge: " + fromId + "->" +toId );
        } catch (ForcedColorException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void decreaseWeight(int fromId, int toId) throws GraphException, NegativeWeightException {
        try {
            Integer currentWeight = graph.getWeight(fromId, toId);
            if (currentWeight <= 0) {
                throw new NegativeWeightException(fromId, toId);
            }
            graph.setWeight(fromId, toId, currentWeight - 1);
            checkColorChange(toId);
        } catch (InvalidEdgeException e) {
            throw new GraphException("Invalid edge: " + fromId + " -> " + toId);
        } catch (ForcedColorException e) {
            throw new RuntimeException(e);
        }
    }

    private void checkColorChange(int nodeId) throws InvalidNodeException, ForcedColorException {
        Color currentNodeColor = graph.getData(nodeId);
        Set<Integer> incomingNeighbors = graph.getIncomingNeighbors(nodeId);

        int totalWeight = 0;
        int colorWeight = 0;

        for (Integer neighborId : incomingNeighbors) {
            try {
                totalWeight += graph.getWeight(neighborId, nodeId);
            } catch (InvalidEdgeException e) {
                throw new RuntimeException(e);
            }
            if (graph.getData(neighborId) == currentNodeColor) {
                try {
                    colorWeight += graph.getWeight(neighborId, nodeId);
                } catch (InvalidEdgeException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        if (colorWeight > totalWeight / 2) {
            for (Color color : Color.values()) {
                if (color != Color.WHITE && color != currentNodeColor) {
                    graph.setData(nodeId, color);
                    throw new ForcedColorException(nodeId, color);
                }
            }
        }
    }
}

