package de.rptu.programmierpraktikum2023.p2.main;

import de.rptu.programmierpraktikum2023.p2.a1.Graph;
import de.rptu.programmierpraktikum2023.p2.a1.GraphImpl;
import de.rptu.programmierpraktikum2023.p2.a2.Color;
import de.rptu.programmierpraktikum2023.p2.a2.GameMove;
import de.rptu.programmierpraktikum2023.p2.a2.GameMoveImpl;
import de.rptu.programmierpraktikum2023.p2.a4.ComputerPlayer;
import de.rptu.programmierpraktikum2023.p2.a4.RandomComputerPlayer;

/**
 * Factory class to wrap the constructors of the student's {@link Graph} and {@link GameMove} implementations.
 */
public class Factory {
    public static <D, W> Graph<D, W> constructGraph() {
        return new GraphImpl<>();
    }

    public static GameMove constructGameMove(Graph<Color, Integer> graph) {
        return new GameMoveImpl(graph);
    }

    public static ComputerPlayer constructComputerPlayer() {
        return new RandomComputerPlayer();
    }
}
