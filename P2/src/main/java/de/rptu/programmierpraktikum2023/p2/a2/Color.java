package de.rptu.programmierpraktikum2023.p2.a2;

public enum Color {
    WHITE, RED, GREEN, BLUE, YELLOW
}
