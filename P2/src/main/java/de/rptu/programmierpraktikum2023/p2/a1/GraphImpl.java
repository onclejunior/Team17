package de.rptu.programmierpraktikum2023.p2.a1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class GraphImpl<D, W> implements Graph<D, W> {

    private ArrayList<Node> matrixNodes = new ArrayList<>();
    private ArrayList<Edge> matrixEdges = new ArrayList<>();


    private Boolean invalidNodeTest(int nodeId){
        return (nodeId < 0 || nodeId >= matrixNodes.size());
    }



    @Override
    public int addNode(D data) {
        Node n = new Node(matrixNodes.size(), data);
        matrixNodes.add(n);
        return n.getId();
    }



    @Override
    public D getData(int nodeId) throws InvalidNodeException {
        if (invalidNodeTest(nodeId)) {
            throw new InvalidNodeException(nodeId);
        } else {
            return (D) matrixNodes.get(nodeId).getData();
        }
    }




    @Override
    public void setData(int nodeId, D data) throws InvalidNodeException {
        if (invalidNodeTest(nodeId)) {
            throw new InvalidNodeException(nodeId);
        } else {
            matrixNodes.set(nodeId, new Node(nodeId, data));
        }
    }


    @Override
    public void addEdge(int fromId, int toId, W weight) throws InvalidNodeException, DuplicateEdgeException {
        if (invalidNodeTest(fromId)) {
            throw new InvalidNodeException(fromId);
        } else if (invalidNodeTest(toId)) {
            throw new InvalidNodeException(toId);
        } else {
            if (getEdge(fromId, toId) == null){
                matrixEdges.add(new Edge(fromId, toId, weight));
            }else{
                throw new DuplicateEdgeException(fromId, toId);
            }
        }
    }


    public Edge getEdge(int fromId, int toId){
        for (Edge e : matrixEdges) {
            if (e.getFromId() == fromId && e.getToId() == toId) {
                return e;
            }
        }
        return null;
    }




    @Override
    public W getWeight(int fromId, int toId) throws InvalidEdgeException {
        Edge e = getEdge(fromId, toId);
        if (e == null) {
            throw new InvalidEdgeException(fromId, toId);
        }else{
            return (W) e.getWeight();
        }

    }

    @Override
    public void setWeight(int fromId, int toId, W weight) throws InvalidEdgeException {
        Edge e = getEdge(fromId, toId);
        if (e == null) {
            throw new InvalidEdgeException(fromId, toId);
        }else{
                matrixEdges.set(matrixEdges.indexOf(e), new Edge(fromId, toId, weight));
        }
    }


    @Override
    public Set<Integer> getNodeIds() {
        Set<Integer> set = new HashSet<Integer>();
        for(Node n : matrixNodes){
            set.add(n.getId());
        }
        return set;
    }


    @Override
    public Set<Integer> getIncomingNeighbors(int nodeId) throws InvalidNodeException {
        if (invalidNodeTest(nodeId)) {
            throw new InvalidNodeException(nodeId);
        } else {
            Set<Integer> set = new HashSet<Integer>();
            for (Edge e : matrixEdges) {
                if (e.getToId() == nodeId) {
                    set.add(e.getFromId());
                }
            }
            return set;
        }
    }


    @Override
    public Set<Integer> getOutgoingNeighbors(int nodeId) throws InvalidNodeException {
        if (invalidNodeTest(nodeId)) {
            throw new InvalidNodeException(nodeId);
        } else {
            Set<Integer> set = new HashSet<Integer>();
            for (Edge e : matrixEdges) {
                if (e.getFromId() == nodeId) {
                    set.add(e.getToId());
                }
            }
            return set;
        }
    }
}
