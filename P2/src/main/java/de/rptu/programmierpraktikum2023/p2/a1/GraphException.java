package de.rptu.programmierpraktikum2023.p2.a1;

public class GraphException extends Exception {
    public GraphException(String message) {
        super(message);
    }
}
