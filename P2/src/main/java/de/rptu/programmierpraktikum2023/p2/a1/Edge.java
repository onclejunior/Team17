package de.rptu.programmierpraktikum2023.p2.a1;

public class Edge<W> {
    private int fromId;
    private int toId;
    public W weight;

    public Edge(int fromID, int toID, W Weight) {
        this.fromId = fromID;
        this.toId = toID;
        this.weight = Weight;
    }
    public int getFromId(){return this.fromId;}
    public int getToId(){return this.toId;}
    public W getWeight(){return this.weight;}
    public void setWeight(W weight){ this.weight = weight;}
}