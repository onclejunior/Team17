package de.rptu.programmierpraktikum2023.p2.a2;

import de.rptu.programmierpraktikum2023.p2.a1.*;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class GameMoveTest {

    @Test
    public void setup() throws Exception {

        GraphImpl<Color, Integer> graph = new GraphImpl<>();
        GameMoveImpl gameMove = new GameMoveImpl(graph);


        graph.addNode(Color.WHITE);//Node a
        graph.addNode(Color.WHITE);//Node b
        graph.addNode(Color.WHITE);//Node c

        graph.addEdge(0, 1, 2);//Edge a -> b
        graph.addEdge(1, 0, 3);//Edge b -> a
        graph.addEdge(2, 0, 1);//Edge c -> a
        graph.addEdge(2, 1, 2);//Edge c -> b


        // Überprüfen, ob alle Knoten Weiß sind
        Set<Integer> nodeIds = graph.getNodeIds();
        for (int nodeId : nodeIds) {
            try {
                Color color = graph.getData(nodeId);
                assertEquals(Color.WHITE, color);
            } catch (InvalidNodeException e) {
                e.printStackTrace();
            }
        }


        // Setzen der Farbe eines Knotens auf Rot
        try {
            gameMove.setColor(2, Color.RED);
            assertEquals(Color.WHITE, graph.getData(0));
            assertEquals(Color.WHITE, graph.getData(1));
            assertEquals(Color.RED, graph.getData(2));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Erhöhen des Gewichts einer Kante
        try {
            gameMove.increaseWeight(2, 1);
            assertEquals(3, graph.getWeight(2, 1));
            assertEquals(Color.RED, graph.getData(0));
            assertEquals(Color.RED, graph.getData(1));
            assertEquals(Color.RED, graph.getData(2));
        } catch (GraphException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Rot
        try {
            gameMove.setColor(2, Color.RED);
            assertEquals(Color.RED, graph.getData(0));
            assertEquals(Color.RED, graph.getData(1));
            assertEquals(Color.RED, graph.getData(2));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Grün
        try {
            gameMove.setColor(2, Color.GREEN);
            assertEquals(Color.GREEN, graph.getData(0));
            assertEquals(Color.GREEN, graph.getData(1));
            assertEquals(Color.GREEN, graph.getData(2));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Verringern des Gewichts einer Kante
        try {
            gameMove.decreaseWeight(2, 0);
            int weight = graph.getWeight(2, 0);
            assertEquals(0, weight);
            assertEquals(Color.GREEN, graph.getData(0));
            assertEquals(Color.GREEN, graph.getData(1));
            assertEquals(Color.GREEN, graph.getData(2));
        } catch (GraphException | NegativeWeightException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void setup2() throws Exception {
        GraphImpl<Color, Integer> graph = new GraphImpl<>();
        GameMoveImpl gameMove = new GameMoveImpl(graph);


        // Erstellen eines sinnvollen Graphen für den Anfangszustand des Spiels
        graph.addNode(Color.WHITE);// Node a
        graph.addNode(Color.WHITE);// Node b
        graph.addNode(Color.WHITE);// Node c
        graph.addNode(Color.WHITE);// Node d

        graph.addEdge(0, 1, 1);// Edge a -> b
        graph.addEdge(0, 2, 1);// Edge a -> c
        graph.addEdge(2, 2, 3);// Edge c -> c
        graph.addEdge(2, 1, 3);// Edge c -> b
        graph.addEdge(3, 1, 1);// Edge d -> b
        graph.addEdge(3, 2, 1);// Edge d -> c


        // Überprüfen, ob alle Knoten die Farbe Weiß haben
        Set<Integer> nodeIds = graph.getNodeIds();
        for (int nodeId : nodeIds) {
            try {
                Color color = graph.getData(nodeId);
                assertEquals(Color.WHITE, color);
            } catch (InvalidNodeException e) {
                e.printStackTrace();
            }
        }


        // Setzen der Farbe eines Knotens auf Rot
        try {
            gameMove.setColor(0, Color.RED);
            assertEquals(Color.RED, graph.getData(0));
            assertEquals(Color.WHITE, graph.getData(1));
            assertEquals(Color.WHITE, graph.getData(2));
            assertEquals(Color.WHITE, graph.getData(3));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Grün
        try {
            gameMove.setColor(2, Color.GREEN);
            assertEquals(Color.RED, graph.getData(0));
            assertEquals(Color.GREEN, graph.getData(1));
            assertEquals(Color.GREEN, graph.getData(2));
            assertEquals(Color.WHITE, graph.getData(3));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Rot
        try {
            gameMove.setColor(3, Color.RED);
            assertEquals(Color.RED, graph.getData(0));
            assertEquals(Color.GREEN, graph.getData(1));
            assertEquals(Color.GREEN, graph.getData(2));
            assertEquals(Color.RED, graph.getData(3));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Verringern des Gewichts einer Kante
        try {
            gameMove.decreaseWeight(2, 1);
            int weight = graph.getWeight(2, 1);
            assertEquals(2, weight);
            assertEquals(Color.RED, graph.getData(0));
            assertEquals(Color.GREEN, graph.getData(1));
            assertEquals(Color.GREEN, graph.getData(2));
            assertEquals(Color.RED, graph.getData(3));
        } catch (GraphException | NegativeWeightException e) {
            e.printStackTrace();
        }


        // Verringern des Gewichts einer Kante
        try {
            gameMove.decreaseWeight(2, 1);
            int weight = graph.getWeight(2, 1);
            assertEquals(1, weight);
            assertEquals(Color.RED, graph.getData(0));
            assertEquals(Color.RED, graph.getData(1));
            assertEquals(Color.GREEN, graph.getData(2));
            assertEquals(Color.RED, graph.getData(3));
        } catch (GraphException | NegativeWeightException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void setup3() throws Exception {
        GraphImpl<Color, Integer> graph = new GraphImpl<>();
        GameMoveImpl gameMove = new GameMoveImpl(graph);
        // Erstellen eines sinnvollen Graphen für den Anfangszustand des Spiels
        graph.addNode(Color.WHITE);// Node a
        graph.addNode(Color.WHITE);// Node b
        graph.addNode(Color.WHITE);// Node c
        graph.addNode(Color.WHITE);// Node d

        graph.addEdge(0, 1, 1);// Edge a -> b
        graph.addEdge(0, 2, 1);// Edge a -> c
        graph.addEdge(2, 2, 2);// Edge c -> c
        graph.addEdge(2, 1, 2);// Edge c -> b
        graph.addEdge(3, 1, 2);// Edge d -> b
        graph.addEdge(3, 2, 1);// Edge d -> c


        // Überprüfen, ob alle Knoten die Farbe Weiß haben
        Set<Integer> nodeIds = graph.getNodeIds();
        for (int nodeId : nodeIds) {
            try {
                Color color = graph.getData(nodeId);
                assertEquals(Color.WHITE, color);
            } catch (InvalidNodeException e) {
                e.printStackTrace();
            }
        }


        // Setzen der Farbe eines Knotens auf Rot
        try {
            gameMove.setColor(3, Color.RED);
            assertEquals(Color.WHITE, graph.getData(0));
            assertEquals(Color.WHITE, graph.getData(1));
            assertEquals(Color.WHITE, graph.getData(2));
            assertEquals(Color.RED, graph.getData(3));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Grün
        try {
            gameMove.setColor(2, Color.GREEN);
            assertEquals(Color.WHITE, graph.getData(0));
            assertEquals(Color.WHITE, graph.getData(1));
            assertEquals(Color.GREEN, graph.getData(2));
            assertEquals(Color.RED, graph.getData(3));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Blau
        try {
            gameMove.setColor(1, Color.BLUE);
            assertEquals(Color.WHITE, graph.getData(0));
            assertEquals(Color.BLUE, graph.getData(1));
            assertEquals(Color.GREEN, graph.getData(2));
            assertEquals(Color.RED, graph.getData(3));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Gelb
        try {
            gameMove.setColor(1, Color.YELLOW);
            assertEquals(Color.WHITE, graph.getData(0));
            assertEquals(Color.YELLOW, graph.getData(1));
            assertEquals(Color.GREEN, graph.getData(2));
            assertEquals(Color.RED, graph.getData(3));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Rot
        try {
            gameMove.setColor(0, Color.RED);
            assertEquals(Color.RED, graph.getData(0));
            assertEquals(Color.RED, graph.getData(1));
            assertEquals(Color.GREEN, graph.getData(2));
            assertEquals(Color.RED, graph.getData(3));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Erhöhen des Gewichts einer Kante
        try {
            gameMove.increaseWeight(2, 2);
            int weight = graph.getWeight(2, 2);
            assertEquals(3, weight);
            assertEquals(Color.RED, graph.getData(0));
            assertEquals(Color.RED, graph.getData(1));
            assertEquals(Color.GREEN, graph.getData(2));
            assertEquals(Color.RED, graph.getData(3));
        } catch (GraphException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Blau
        try {
            gameMove.setColor(3, Color.BLUE);
            assertEquals(Color.RED, graph.getData(0));
            assertEquals(Color.RED, graph.getData(1));
            assertEquals(Color.GREEN, graph.getData(2));
            assertEquals(Color.BLUE, graph.getData(3));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Gelb
        try {
            gameMove.setColor(0, Color.YELLOW);
            assertEquals(Color.YELLOW, graph.getData(0));
            assertEquals(Color.RED, graph.getData(1));
            assertEquals(Color.GREEN, graph.getData(2));
            assertEquals(Color.BLUE, graph.getData(3));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Rot
        try {
            gameMove.setColor(3, Color.RED);
            assertEquals(Color.YELLOW, graph.getData(0));
            assertEquals(Color.RED, graph.getData(1));
            assertEquals(Color.GREEN, graph.getData(2));
            assertEquals(Color.RED, graph.getData(3));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Erhöhen des Gewichts einer Kante
        try {
            gameMove.increaseWeight(2, 1);
            int weight = graph.getWeight(2, 1);
            assertEquals(3, weight);
            assertEquals(Color.YELLOW, graph.getData(0));
            assertEquals(Color.RED, graph.getData(1));
            assertEquals(Color.GREEN, graph.getData(2));
            assertEquals(Color.RED, graph.getData(3));
        } catch (GraphException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Blau
        try {
            gameMove.setColor(1, Color.BLUE);
            assertEquals(Color.YELLOW, graph.getData(0));
            assertEquals(Color.BLUE, graph.getData(1));
            assertEquals(Color.GREEN, graph.getData(2));
            assertEquals(Color.RED, graph.getData(3));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Gelb
        try {
            gameMove.setColor(3, Color.YELLOW);
            assertEquals(Color.YELLOW, graph.getData(0));
            assertEquals(Color.BLUE, graph.getData(1));
            assertEquals(Color.GREEN, graph.getData(2));
            assertEquals(Color.YELLOW, graph.getData(3));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Rot
        try {
            gameMove.setColor(3, Color.RED);
            assertEquals(Color.YELLOW, graph.getData(0));
            assertEquals(Color.BLUE, graph.getData(1));
            assertEquals(Color.GREEN, graph.getData(2));
            assertEquals(Color.RED, graph.getData(3));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Erhöhen des Gewichts einer Kante
        try {
            gameMove.increaseWeight(2, 1);
            int weight = graph.getWeight(2, 1);
            assertEquals(4, weight);
            assertEquals(Color.YELLOW, graph.getData(0));
            assertEquals(Color.GREEN, graph.getData(1));
            assertEquals(Color.GREEN, graph.getData(2));
            assertEquals(Color.RED, graph.getData(3));
        } catch (GraphException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void setup4() throws Exception {
        GraphImpl<Color, Integer> graph = new GraphImpl<>();
        GameMoveImpl gameMove = new GameMoveImpl(graph);
        // Erstellen eines sinnvollen Graphen für den Anfangszustand des Spiels
        graph.addNode(Color.WHITE);// Node a
        graph.addNode(Color.WHITE);// Node b

        graph.addEdge(0, 1, 1);// Edge a -> b


        // Überprüfen, ob alle Knoten die Farbe Weiß haben
        Set<Integer> nodeIds = graph.getNodeIds();
        for (int nodeId : nodeIds) {
            try {
                Color color = graph.getData(nodeId);
                assertEquals(Color.WHITE, color);
            } catch (InvalidNodeException e) {
                e.printStackTrace();
            }
        }


        // Setzen der Farbe eines Knotens auf Rot
        try {
            gameMove.setColor(0, Color.RED);
            assertEquals(Color.RED, graph.getData(0));
            assertEquals(Color.RED, graph.getData(1));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Rot
        try {
            gameMove.setColor(0, Color.RED);
            assertEquals(Color.RED, graph.getData(0));
            assertEquals(Color.RED, graph.getData(1));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void setup5() throws Exception {

        GraphImpl<Color, Integer> graph = new GraphImpl<>();
        GameMoveImpl gameMove = new GameMoveImpl(graph);

        // Erstellen eines sinnvollen Graphen für den Anfangszustand des Spiels
        graph.addNode(Color.WHITE);// Node a
        graph.addEdge(0, 0, 1);// Edge a -> b


        // Überprüfen, ob alle Knoten die Farbe Weiß haben
        Set<Integer> nodeIds = graph.getNodeIds();
        for (int nodeId : nodeIds) {
            try {
                Color color = graph.getData(nodeId);
                assertEquals(Color.WHITE, color);
            } catch (InvalidNodeException e) {
                e.printStackTrace();
            }
        }


        // Setzen der Farbe eines Knotens auf Rot
        try {
            gameMove.setColor(0, Color.RED);
            assertEquals(Color.RED, graph.getData(0));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Rot
        try {
            gameMove.setColor(0, Color.RED);
            assertEquals(Color.RED, graph.getData(0));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void setup5b()  throws Exception {
        GraphImpl<Color, Integer>  graph = new GraphImpl<Color, Integer>(){
            Color a = Color.WHITE;
            int e = 1;


            @Override
            public int addNode(Color data) {
                throw new UnsupportedOperationException();
            }

            @Override
            public Color getData(int nodeId) throws InvalidNodeException {
                if (nodeId == 0) return a;
                throw new InvalidNodeException(nodeId);
            }

            @Override
            public void setData(int nodeId, Color data) throws InvalidNodeException {
                if (nodeId == 0)  a = data;
                throw new InvalidNodeException(nodeId);
            }

            @Override
            public void addEdge(int fromId, int toId, Integer weight) throws InvalidNodeException, DuplicateEdgeException {
                throw new UnsupportedOperationException();
            }

            @Override
            public Integer getWeight(int fromId, int toId) throws InvalidEdgeException {
                if (fromId == 0 && toId == 0)return 1;
                throw new InvalidEdgeException(fromId, toId);
            }

            @Override
            public void setWeight(int fromId, int toId, Integer weight) throws InvalidEdgeException {
                if (fromId == 0 && toId == 0) e = weight;
                throw new InvalidEdgeException(fromId, toId);
            }

            @Override
            public Set<Integer> getNodeIds() {
                return Set.of(0);
            }

            @Override
            public Set<Integer> getIncomingNeighbors(int nodeId) throws InvalidNodeException {
                if (nodeId == 0) return Set.of();
                throw new InvalidNodeException(nodeId);
            }

            @Override
            public Set<Integer> getOutgoingNeighbors(int nodeId) throws InvalidNodeException {
                if (nodeId == 0) return Set.of();
                throw new InvalidNodeException(nodeId);
            }
        };
        // Setzen der Farbe eines Knotens auf Rot
        GameMove gameMove = new GameMoveImpl(graph);

        try {
            gameMove.setColor(0, Color.RED);
            assertEquals(Color.RED, graph.getData(0));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Rot
        try {
            gameMove.setColor(0, Color.RED);
            assertEquals(Color.RED, graph.getData(0));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void setup6() throws Exception {
        GraphImpl<Color, Integer> graph = new GraphImpl<>();
        GameMoveImpl gameMove = new GameMoveImpl(graph);
        // Erstellen eines sinnvollen Graphen für den Anfangszustand des Spiels
        graph.addNode(Color.WHITE);// Node a

        graph.addEdge(0, 0, 1);// Edge a -> a


        // Überprüfen, ob alle Knoten die Farbe Weiß haben
        Set<Integer> nodeIds = graph.getNodeIds();
        for (int nodeId : nodeIds) {
            try {
                Color color = graph.getData(nodeId);
                assertEquals(Color.WHITE, color);
            } catch (InvalidNodeException e) {
                e.printStackTrace();
            }
        }


        // Setzen der Farbe eines Knotens auf Rot
        try {
            gameMove.setColor(0, Color.RED);
            assertEquals(Color.RED, graph.getData(0));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Verringern des Gewichts einer Kante
        try {
            gameMove.decreaseWeight(0, 0);
            int weight = graph.getWeight(0, 0);
            assertEquals(0, weight);
            assertEquals(Color.RED, graph.getData(0));
        } catch (GraphException | NegativeWeightException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Rot
        try {
            gameMove.setColor(0, Color.RED);
            assertEquals(Color.RED, graph.getData(0));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Grün
        try {
            gameMove.setColor(0, Color.GREEN);
            assertEquals(Color.GREEN, graph.getData(0));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void setup6b()  throws Exception {
        GraphImpl<Color, Integer>  graph = new GraphImpl<Color, Integer>(){
            Color a = Color.WHITE;
            int e = 1;


            @Override
            public int addNode(Color data) {
                throw new UnsupportedOperationException();
            }

            @Override
            public Color getData(int nodeId) throws InvalidNodeException {
                if (nodeId == 0) {return a;}else{
                throw new InvalidNodeException(nodeId);}
            }

            @Override
            public void setData(int nodeId, Color data) throws InvalidNodeException {
                if (nodeId == 0)  a = data;
                throw new InvalidNodeException(nodeId);
            }

            @Override
            public void addEdge(int fromId, int toId, Integer weight) throws InvalidNodeException, DuplicateEdgeException {
                throw new UnsupportedOperationException();
            }

            @Override
            public Integer getWeight(int fromId, int toId) throws InvalidEdgeException {
                if (fromId == 0 && toId == 0){return 1;}else{
                throw new InvalidEdgeException(fromId, toId);}
            }

            @Override
            public void setWeight(int fromId, int toId, Integer weight) throws InvalidEdgeException {
                if (fromId == 0 && toId == 0) {e = weight;}else{
                throw new InvalidEdgeException(fromId, toId);}
            }

            @Override
            public Set<Integer> getNodeIds() {
                return Set.of(0);
            }

            @Override
            public Set<Integer> getIncomingNeighbors(int nodeId) throws InvalidNodeException {
                if (nodeId == 0) return Set.of();
                throw new InvalidNodeException(nodeId);
            }

            @Override
            public Set<Integer> getOutgoingNeighbors(int nodeId) throws InvalidNodeException {
                if (nodeId == 0) return Set.of();
                throw new InvalidNodeException(nodeId);
            }
        };
        GameMove gameMove = new GameMoveImpl(graph);

        // Verringern des Gewichts einer Kante
        try {
            gameMove.decreaseWeight(0, 0);
            int weight = graph.getWeight(0, 0);
            assertEquals(0, weight);
            assertEquals(Color.RED, graph.getData(0));
        } catch (GraphException | NegativeWeightException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Rot
        try {
            gameMove.setColor(0, Color.RED);
            assertEquals(Color.RED, graph.getData(0));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }


        // Setzen der Farbe eines Knotens auf Grün
        try {
            gameMove.setColor(0, Color.GREEN);
            assertEquals(Color.GREEN, graph.getData(0));
        } catch (GraphException | ForcedColorException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void setup7() throws Exception {
        GraphImpl<Color, Integer> graph = new GraphImpl<>();
        GameMoveImpl gameMove = new GameMoveImpl(graph);
        // Erstellen eines sinnvollen Graphen für den Anfangszustand des Spiels
        graph.addNode(Color.WHITE);// Node a
        graph.addEdge(0, 0, 1);// Edge a -> b


        // Überprüfen, ob alle Knoten die Farbe Weiß haben
        Set<Integer> nodeIds = graph.getNodeIds();
        for (int nodeId : nodeIds) {
            try {
                Color color = graph.getData(nodeId);
                assertEquals(Color.WHITE, color);
            } catch (InvalidNodeException e) {
                e.printStackTrace();
            }
        }


        // Verringern des Gewichts einer Kante
        try {
            gameMove.decreaseWeight(0, 0);
            int weight = graph.getWeight(0, 0);
            assertEquals(0, weight);
            assertEquals(Color.WHITE, graph.getData(0));
        } catch (GraphException | NegativeWeightException e) {
            e.printStackTrace();
        }


        // Verringern des Gewichts einer Kante
        try {
            gameMove.decreaseWeight(0, 0);
            int weight = graph.getWeight(0, 0);
            assertEquals(0, weight);
            assertEquals(Color.WHITE, graph.getData(0));
        } catch (GraphException | NegativeWeightException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void setup7b()  throws Exception {
        GraphImpl<Color, Integer>  graph = new GraphImpl<Color, Integer>(){
            Color a = Color.WHITE;
            int e = 1;


            @Override
            public int addNode(Color data) {
                throw new UnsupportedOperationException();
            }

            @Override
            public Color getData(int nodeId) throws InvalidNodeException {
                if (nodeId == 0) return a;
                throw new InvalidNodeException(nodeId);
            }

            @Override
            public void setData(int nodeId, Color data) throws InvalidNodeException {
                if (nodeId == 0)  a = data;
                throw new InvalidNodeException(nodeId);
            }

            @Override
            public void addEdge(int fromId, int toId, Integer weight) throws InvalidNodeException, DuplicateEdgeException {
                throw new UnsupportedOperationException();
            }

            @Override
            public Integer getWeight(int fromId, int toId) throws InvalidEdgeException {
                if (fromId == 0 && toId == 0)return 1;
                throw new InvalidEdgeException(fromId, toId);
            }

            @Override
            public void setWeight(int fromId, int toId, Integer weight) throws InvalidEdgeException {
                if (fromId == 0 && toId == 0) e = weight;
                throw new InvalidEdgeException(fromId, toId);
            }

            @Override
            public Set<Integer> getNodeIds() {
                return Set.of(0);
            }

            @Override
            public Set<Integer> getIncomingNeighbors(int nodeId) throws InvalidNodeException {
                if (nodeId == 0) return Set.of();
                throw new InvalidNodeException(nodeId);
            }

            @Override
            public Set<Integer> getOutgoingNeighbors(int nodeId) throws InvalidNodeException {
                if (nodeId == 0) return Set.of();
                throw new InvalidNodeException(nodeId);
            }
        };
        // Setzen der Farbe eines Knotens auf Rot
        GameMove gameMove = new GameMoveImpl(graph);
        // Verringern des Gewichts einer Kante
        try {
            gameMove.decreaseWeight(0, 0);
            int weight = graph.getWeight(0, 0);
            assertEquals(0, weight);
            assertEquals(Color.WHITE, graph.getData(0));
        } catch (GraphException | NegativeWeightException e) {
            e.printStackTrace();
        }


        // Verringern des Gewichts einer Kante
        try {
            gameMove.decreaseWeight(0, 0);
            int weight = graph.getWeight(0, 0);
            assertEquals(0, weight);
            assertEquals(Color.WHITE, graph.getData(0));
        } catch (GraphException | NegativeWeightException e) {
            e.printStackTrace();
        }
    }
}