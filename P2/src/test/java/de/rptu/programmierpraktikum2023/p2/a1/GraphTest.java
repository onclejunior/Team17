package de.rptu.programmierpraktikum2023.p2.a1;

import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;



public class GraphTest {

    Graph<String, Double> graph = new GraphImpl<>();

    @Test
    public void example() throws Exception {

        //Example
        assertTrue(graph.getNodeIds().isEmpty());
        assertThrows(InvalidNodeException.class, () -> graph.addEdge(0, 0, 69.0));
    }

    @Test
    public void GraphTestAll() throws Exception {
        //addNode
        graph.addNode("0");
        graph.addNode("1");
        graph.addNode("2");
        graph.addNode("1312");
        graph.addNode("acdc");


        //addEdge
        graph.addEdge(0,1,69.0);
        graph.addEdge(0,0,7.0);
        graph.addEdge(3,2,9.2);
        graph.addEdge(2,1,0.0);
        graph.addEdge(2,4,3.141592);
        assertThrows(InvalidNodeException.class, () -> graph.addEdge(-1,0,18.7));
        assertThrows(InvalidNodeException.class, () -> graph.addEdge(0,15,3.3));
        assertThrows(DuplicateEdgeException.class, () -> graph.addEdge(0,1,0.123));
        assertThrows(DuplicateEdgeException.class, () -> graph.addEdge(0,0,7.0));

        //getData
        assertEquals(graph.getData(0), "0");
        assertEquals(graph.getData(1), "1");
        assertEquals(graph.getData(3), "1312");
        assertThrows(InvalidNodeException.class, () -> graph.getData(6));
        assertThrows(InvalidNodeException.class, () -> graph.getData(-1));

        //setData
        assertThrows(InvalidNodeException.class, () -> graph.setData(7, "abc"));
        assertThrows(InvalidNodeException.class, () -> graph.setData(-13, "not13"));
        graph.setData(0, "0.000");
        graph.setData(1, "0.999... = 1");

        //getWeight
        assertThrows(InvalidEdgeException.class, () -> graph.getWeight(7,1));
        assertThrows(InvalidEdgeException.class, () -> graph.getWeight(0,-3));
        assertThrows(InvalidEdgeException.class, () -> graph.getWeight(0,2));
        assertThrows(InvalidEdgeException.class, () -> graph.getWeight(2,3));
        assertEquals(graph.getWeight(0,0), 7.0);
        assertEquals(graph.getWeight(2,4), 3.141592);

        //setWeight
        assertThrows(InvalidEdgeException.class, () -> graph.setWeight(7,1, 0.187));
        assertThrows(InvalidEdgeException.class, () -> graph.setWeight(0,-3, 333.0));
        assertThrows(InvalidEdgeException.class, () -> graph.setWeight(0,2,13.0));
        assertThrows(InvalidEdgeException.class, () -> graph.setWeight(2,3, 187.0));
        graph.setWeight(0,0, 420.0);
        graph.setWeight(2,4, 3.1415);



        //testSet init -- getNodeIds
        Set<Integer> testSet = new HashSet();
        testSet.add(0);
        testSet.add(1);
        testSet.add(2);
        testSet.add(3);
        testSet.add(4);
        //getNodeIds
        assertEquals(graph.getNodeIds(), testSet);



        //testSet init -- getIncomingNeighbors
        testSet.remove(1);
        testSet.remove(3);
        testSet.remove(4);
        //getIncomingNeighbors
        assertEquals(graph.getIncomingNeighbors(1),testSet);
        assertThrows(InvalidNodeException.class, () -> graph.getIncomingNeighbors(-1));
        assertThrows(InvalidNodeException.class, () -> graph.getIncomingNeighbors(5));




        //testSet init -- getOutgoingNeighbors
        testSet.remove(0);
        testSet.remove(2);
        testSet.add(1);
        testSet.add(4);
        //getOutgoingNeighbors
        assertEquals(graph.getOutgoingNeighbors(2), testSet);
        assertThrows(InvalidNodeException.class, () -> graph.getOutgoingNeighbors(5));
        assertThrows(InvalidNodeException.class, () -> graph.getOutgoingNeighbors(-5));



        //testSet init -- getIncomingNeighbors,getOutgoingNeighbors == null
        testSet.remove(1);
        testSet.remove(4);
        assertEquals(graph.getIncomingNeighbors(3), testSet);
        assertEquals(graph.getOutgoingNeighbors(1), testSet);


    }
}
