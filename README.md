Programmierpraktikum SS23 - Team 17
===================================

Team
----

- [Junior Gogang Kengne](mailto:kengne@rhrk.uni-kl.de)
- [Tim Burchert](mailto:burchert@rhrk.uni-kl.de)
- [Jens Habermehl](mailto:habermeh@rhrk.uni-kl.de)
- [Elias Konrad](mailto:sym56wyl@rhrk.uni-kl.de)


Betreuung
---------

- Tutor: Jordan Gwenet
- Zeitslot für Abnahmen: Donnerstag 14Uhr(Abnahme P1: 06.07.)


PP Website
----------

https://pl.cs.uni-kl.de/homepage/de/teaching/ss23/pp/#registrierung
