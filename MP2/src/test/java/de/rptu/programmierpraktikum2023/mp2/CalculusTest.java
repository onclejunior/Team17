package de.rptu.programmierpraktikum2023.mp2;

import de.rptu.programmierpraktikum2023.mp2.antlr.FunctionLexer;
import de.rptu.programmierpraktikum2023.mp2.antlr.FunctionParser;
import de.rptu.programmierpraktikum2023.mp2.antlr.FunctionVisitor;
import de.rptu.programmierpraktikum2023.mp2.functions.Function;
import de.rptu.programmierpraktikum2023.mp2.functions.MathUtils;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.math.BigDecimal;
import java.util.Random;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static de.rptu.programmierpraktikum2023.mp2.functions.MathUtils.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CalculusTest {
    private static final String packageName = Function.class.getPackageName();
    private static final String calculusPackageName = Calculus.class.getPackageName();

    private static class FunctionWithRef {
        final String functionStr;
        final java.util.function.Function<BigDecimal, BigDecimal> ref;  // reference implementation for f
        final java.util.function.Function<BigDecimal, BigDecimal> dref; // reference implementation for f'

        FunctionWithRef(String functionStr, java.util.function.Function<BigDecimal, BigDecimal> ref, java.util.function.Function<BigDecimal, BigDecimal> dref) {
            this.functionStr = functionStr;
            this.ref = ref;
            this.dref = dref;
        }
    }

    private static Stream<FunctionWithRef> getFunctionInstances() {
        Stream.Builder<FunctionWithRef> sb = Stream.builder();

        sb.add(new FunctionWithRef(
                "42",
                x -> new BigDecimal(42),
                x -> BigDecimal.ZERO
        ));
        sb.add(new FunctionWithRef(
                "x+3",
                x -> add(x, new BigDecimal(3)),
                x -> BigDecimal.ONE
        ));
        sb.add(new FunctionWithRef(
                "x*x",
                x -> x.pow(2),
                x -> multiply(new BigDecimal(2), x)
        ));
        sb.add(new FunctionWithRef(
                "sin(x)",
                MathUtils::sin,
                MathUtils::cos
        ));
        sb.add(new FunctionWithRef(
                "3/x",
                x -> divide(new BigDecimal(3), x),
                x -> divide(new BigDecimal(-3), x.pow(2))
        ));
        sb.add(new FunctionWithRef(
                "3*x+15*sin(x)",
                x -> add(multiply(new BigDecimal(3), x), multiply(new BigDecimal(15), sin(x))),
                x -> add(multiply(new BigDecimal(15), cos(x)), new BigDecimal(3))
        ));
        sb.add(new FunctionWithRef(
                "exp(-x*x)",
                x -> exp(x.pow(2).negate()),
                x -> multiply(new BigDecimal(-2), multiply(exp(x.pow(2).negate()), x))
        ));
        sb.add(new FunctionWithRef(
                "sin(x*x)/(x*x)",
                x -> divide(sin(x.pow(2)), x.pow(2)),
                x -> divide(
                        subtract(
                                multiply(multiply(new BigDecimal(2), x.pow(2)), cos(x.pow(2))),
                                multiply(new BigDecimal(2), sin(x.pow(2)))
                        ),
                        x.pow(3)
                )
        ));
        sb.add(new FunctionWithRef(
                "sin(x)+1/3*sin(3*x)+1/5*sin(5*x)+1/7*sin(7*x)+1/9*sin(9*x)+1/11*sin(11*x)",
                x -> IntStream.of(1, 3, 5, 7, 9, 11)
                        .mapToObj(BigDecimal::new)
                        .map(z -> multiply(divide(BigDecimal.ONE, z), sin(multiply(z, x))))
                        .reduce(MathUtils::add).get(),
                x -> IntStream.of(1, 3, 5, 7, 9, 11)
                        .mapToObj(BigDecimal::new)
                        .map(z -> cos(multiply(z, x)))
                        .reduce(MathUtils::add).get()
        ));
        sb.add(new FunctionWithRef(
                "1/x*1/(x-2)*1/(x-4)*1/(x-6)*1/(x+2)*1/(x+4)*1/(x+6)",
                x -> multiply(
                        IntStream.of(0, 2, 4, 6)
                                .mapToObj(BigDecimal::new)
                                .map(z -> divide(BigDecimal.ONE, subtract(x, z)))
                                .reduce(MathUtils::multiply).get(),
                        IntStream.of(2, 4, 6)
                                .mapToObj(BigDecimal::new)
                                .map(z -> divide(BigDecimal.ONE, add(x, z)))
                                .reduce(MathUtils::multiply).get()
                ),
                x -> divide(
                        Stream.of(
                                multiply(new BigDecimal(-7), x.pow(6)),
                                multiply(new BigDecimal(280), x.pow(4)),
                                multiply(new BigDecimal(-2352), x.pow(2)),
                                new BigDecimal(2304)
                        ).reduce(MathUtils::add).get(),
                        IntStream.of(-6, -4, -2, 0, 2, 4, 6)
                                .mapToObj(BigDecimal::new)
                                .map(z -> add(x, z).pow(2))
                                .reduce(MathUtils::multiply).get()
                )
        ));
        sb.add(new FunctionWithRef(
                "sin(x)*cos(20*x)",
                x -> multiply(sin(x), cos(multiply(new BigDecimal(20), x))),
                x -> subtract(
                        multiply(cos(x), cos(multiply(new BigDecimal(20), x))),
                        multiply(new BigDecimal(20), multiply(sin(x), sin(multiply(new BigDecimal(20), x))))
                )
        ));

        // Optional parts
        try {
            Class.forName(packageName + "." + "Log");
            sb.add(new FunctionWithRef(
                    "log(x*x)",
                    x -> log(x.pow(2)),
                    x -> divide(new BigDecimal(2), x)
            ));
        } catch (ClassNotFoundException e) {
            System.out.println("Überspringe Test für optionalen Teil von Aufgabe 1 (Klasse Log).");
        }

        try {
            Class.forName(packageName + "." + "Pow");
            sb.add(new FunctionWithRef(
                    "(x*x)^3",
                    x -> x.pow(6),
                    x -> multiply(new BigDecimal(6), x.pow(5))
            ));
        } catch (ClassNotFoundException e) {
            System.out.println("Überspringe Test für optionalen Teil von Aufgabe 1 (Klasse Pow).");
        }

        return sb.build();
    }

    private static Stream<Arguments> getFunctionInstancesWithSeed() throws ReflectiveOperationException {
        try {
            FunctionVisitor<?> visitor = (FunctionVisitor<?>) Class.forName(calculusPackageName + "." + "Parser").getConstructor().newInstance();
            return Stream.of(4711, 123, 42).flatMap(seed ->
                    getFunctionInstances().map(fr -> Arguments.of(fr, seed, visitor))
            );
        } catch (ClassNotFoundException e) {
            System.out.println("FEHLER: Klasse Parser im Paket " + calculusPackageName + " nicht gefunden!");
            throw e;
        }
    }

    private static Function parseFunction(String input, FunctionVisitor<Function> visitor) {
        FunctionLexer lexer = new FunctionLexer(CharStreams.fromString(input));
        FunctionParser parser = new FunctionParser(new CommonTokenStream(lexer));
        ParseTree tree = parser.expr();
        return visitor.visit(tree);
    }

    @ParameterizedTest
    @MethodSource("getFunctionInstancesWithSeed")
    public void testFunctions(FunctionWithRef fr, long seed, FunctionVisitor<Function> visitor) {
        System.out.printf("%nTeste die Funktion         f(x) = %s   mit seed %s%n", fr.functionStr, seed);
        Function f = parseFunction(fr.functionStr, visitor);
        assertNotNull(f, "Aufruf des Parsers gibt null zurück!");

        String lpad = "    ";
        System.out.println(lpad + "Ergebnis des Parsers:  f(x) = " + f + "   [bitte manuell mit Funktion darüber vergleichen, Darstellung ist nicht eindeutig]");

        Function fSimplified = f.simplify();
        assertNotNull(fSimplified, "f.simplify() gibt null zurück!");

        if (fSimplified != f) {
            System.out.println(lpad + "Ergebnis von simplify: f(x) = " + fSimplified + "   [bitte manuell mit Funktion darüber vergleichen, Darstellung ist nicht eindeutig]");
        } else {
            System.out.println(lpad + "f.simplify() gibt f zurück (optionale Aufgabe 2 nicht bearbeitet oder keine Vereinfachung gefunden)");
        }

        new Random(seed).doubles(-200, 200).limit(1000).mapToObj(BigDecimal::valueOf).forEach(s -> {
            assertEquals(fr.ref.apply(s).doubleValue(), f.apply(s).doubleValue(), 0.1, lpad + "Die Funktion liefert für x=" + s + " ein falsches Ergebnis!");
        });
        System.out.println(lpad + "Werte der Funktion stimmen mit den Vorgaben überein.");

        if (fSimplified != f) {
            new Random(seed).doubles(-200, 200).limit(1000).mapToObj(BigDecimal::valueOf).forEach(s -> {
                assertEquals(fr.ref.apply(s).doubleValue(), fSimplified.apply(s).doubleValue(), 0.1, lpad + "Die vereinfachte Funktion liefert für x=" + s + " ein falsches Ergebnis!");
            });
            System.out.println(lpad + "Werte der vereinfachten Funktion stimmen mit den Vorgaben überein.");
        }
    }

    @ParameterizedTest
    @MethodSource("getFunctionInstancesWithSeed")
    public void testDerivatives(FunctionWithRef fr, long seed, FunctionVisitor<Function> visitor) {
        System.out.printf("%nTeste die Funktion         f(x)  = %s   mit seed %s%n", fr.functionStr, seed);
        Function f = parseFunction(fr.functionStr, visitor);
        assertNotNull(f, "Aufruf des Parsers gibt null zurück!");

        String lpad = "    ";
        System.out.println(lpad + "Ergebnis des Parsers:  f(x)  = " + f + "   [bitte manuell mit Funktion darüber vergleichen, Darstellung ist nicht eindeutig]");

        Function derivative = f.derive();
        assertNotNull(derivative, "f.derive() gibt null zurück!");
        System.out.println(lpad + "Ableitung:             f'(x) = " + derivative + "   [bitte manuell mit Funktion darüber vergleichen, Darstellung ist nicht eindeutig]");

        Function derivativeSimplified = derivative.simplify();
        assertNotNull(derivativeSimplified, "f.derive().simplify() gibt null zurück!");

        if (derivativeSimplified != derivative) {
            System.out.println(lpad + "Ergebnis von simplify: f'(x) = " + derivativeSimplified + "   [bitte manuell mit Funktion darüber vergleichen, Darstellung ist nicht eindeutig]");
        } else {
            System.out.println(lpad + "f.derive().simplify() gibt f.derive() zurück (optionale Aufgabe 2 nicht bearbeitet oder keine Vereinfachung gefunden)");
        }

        new Random(seed).doubles(-200, 200).limit(1000).mapToObj(BigDecimal::valueOf).forEach(s -> {
            assertEquals(fr.dref.apply(s).doubleValue(), derivative.apply(s).doubleValue(), 0.1, lpad + "Ableitung liefert für x=" + s + " ein falsches Ergebnis!"); // f(x)
        });
        System.out.println(lpad + "Werte der Ableitung stimmen mit den Vorgaben überein.");

        if (derivativeSimplified != derivative) {
            new Random(seed).doubles(-200, 200).limit(1000).mapToObj(BigDecimal::valueOf).forEach(s -> {
                assertEquals(fr.dref.apply(s).doubleValue(), derivativeSimplified.apply(s).doubleValue(), 0.1, lpad + "Vereinfachung der Ableitung liefert für x=" + s + " ein falsches Ergebnis!"); // f(x)
            });
            System.out.println(lpad + "Werte der vereinfachten Ableitung stimmen mit den Vorgaben überein.");
        }
    }
}
