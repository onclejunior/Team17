package de.rptu.programmierpraktikum2023.mp2.functions;

import java.math.BigDecimal;

public class Add implements Function{

    final Function f1;
    final Function f2;

    public Add(Function f1, Function f2) {
        this.f1 = f1;
        this.f2 = f2;
    }

    @Override
    public String toString() {
        return "(" + f1.toString() + " + " + f2.toString() + ")";
    }

    @Override
    public BigDecimal apply(BigDecimal x) {
        return MathUtils.add(f1.apply(x), f2.apply(x));
    }

    @Override
    public Function derive() {
        return new Add(f1.derive(), f2.derive());
    } //ableitung ist summe der ableitungen
}
