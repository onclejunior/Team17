package de.rptu.programmierpraktikum2023.mp2.functions;

import java.math.BigDecimal;

public class Log implements Function {
    Function f1;

    public Log(Function x) {
        this.f1 = x;
    }

    @Override
    public String toString() {

        return "Log( " + this.f1.toString() + " )";
    }

    @Override
    public BigDecimal apply(BigDecimal x) {
        return MathUtils.log(this.f1.apply(x));
    }

    @Override
    public Function derive() {
        //f(x)= ln(1) -> f'(x) = 0
        if(this.f1 == BigDecimal.ONE){
            return new Const(BigDecimal.ZERO);
            //f(x)= ln(e) -> f'(x) = 1
        } else if ((this.f1).equals(Math.E)){
            return new Const(BigDecimal.ONE);

        } else {
            //ln(x) -> 1/X

            Function dm = new Div(new Const(BigDecimal.ONE), this.f1);
            return new Mult(dm,this.f1.derive());
        }
    }

    @Override
    public Function simplify() {
        Function result = new Log(this.f1);


        if ((this.f1 instanceof Const)) {
            // ln(1) = 0.
            if (this.f1.toString().equals("1")) {
                result = new Const(BigDecimal.ZERO);
            }

            // ln(0) = undefined.
            if (this.f1.toString().equals("0")) {
                result = null;
            }
        }
        return result;
    }
}


