package de.rptu.programmierpraktikum2023.mp2.functions;

import java.math.BigDecimal;

public class Cos implements Function{

    final Function f;

    public Cos(Function f) {
        this.f = f;
    }

    @Override
    public String toString() {
        return "cos(" + f.toString() + ")";
    }

    @Override
    public BigDecimal apply(BigDecimal x) {
        return BigDecimal.valueOf(Math.cos(f.apply(x).doubleValue()));
    }

    @Override
    public Function derive() {
        return new Mult(f.derive(), new Mult(new Const(new BigDecimal(-1)), new Sin(f))); //ableitung von cos ist -sin
    }
}
