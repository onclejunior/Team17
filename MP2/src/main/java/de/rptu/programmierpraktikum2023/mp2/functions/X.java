package de.rptu.programmierpraktikum2023.mp2.functions;

import java.math.BigDecimal;

public class X implements Function {

    @Override
    public String toString() {
        return "x";
    }

    @Override
    public BigDecimal apply(BigDecimal x) {
        return x;
    }

    @Override
    public Function derive() { // ableitung von x ist 1
        return new Const(new BigDecimal(1));
    }
}
