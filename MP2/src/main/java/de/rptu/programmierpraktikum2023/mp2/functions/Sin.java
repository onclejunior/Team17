package de.rptu.programmierpraktikum2023.mp2.functions;

import java.math.BigDecimal;

public class Sin implements Function{

    final Function f;

    public Sin(Function f) {
        this.f = f;
    }

    @Override
    public String toString() {
        return "sin(" + f.toString() + ")";
    }

    @Override
    public BigDecimal apply(BigDecimal x) {
        return BigDecimal.valueOf(Math.sin(f.apply(x).doubleValue()));
    }

    @Override // ableitung sin ist cos
    public Function derive() {
        return new Mult(f.derive(), new Cos(f));
    }
}
