package de.rptu.programmierpraktikum2023.mp2;

import de.rptu.programmierpraktikum2023.mp2.functions.*;

import java.io.IOException;
import java.math.BigDecimal;

public class Calculus {
    public static void main(String[] args) throws IOException {


        // -------------------------------------------------------------------------------------------------------------
        // Aufgabe 1
        // Hier die Beispiele aus dem Anhang definieren:

        //Function f = new Const(new BigDecimal ("3.14")); // f(x) = 3.14
        //Function f = new X(); // f(x) = x
        //Function f = new Exp(new X()); // f(x) = Eulersche Zahl e hoch x
        //Function f = new Sin(new X()); // f(x) = sin(x)
        //Function f = new Cos(new X()); // f(x) = cos(x)
        Function f = new Add(new Const(new BigDecimal (42)), new X()); // f(x) = 42 +
        //Function f = new Mult(new Const(new BigDecimal (42)), new X()); // f(x) = 42 +
        //Function f = new Div(new Const(new BigDecimal (42)), new X()); // f(x) = 42 +
        //Function f = ...
        //Function f = ...

        // Ein Beispiel auswählen:
        //f = f0;


        // -------------------------------------------------------------------------------------------------------------
        // Aufgabe 3

        // Zur Laufzeit die Funktion (als String) abfragen und diesen String parsen:
        f = Util.promptFunction(new Parser());

        if (f != null) {
            // Entweder die Grenzen fest vorgeben:
            //BigDecimal xmin = new BigDecimal(-10);
            //BigDecimal xmax = new BigDecimal(10);

            // oder zur Laufzeit abfragen:
            BigDecimal xmin = Util.promptXMin();
            BigDecimal xmax = Util.promptXMax();

            // Eingaben anzeigen:
            System.out.println("\nf(x)  = " + f);
            Function derivative = f.derive();
            if (derivative != null) {
                System.out.println("f'(x) = " + derivative);
                Function simpleDerivative = derivative.simplify();
                if (simpleDerivative != null && simpleDerivative != derivative) {
                    System.out.println("f'(x) = " + simpleDerivative + "    // simplified");
                }
            }

            // Plot
            Util.plotFunction(f, xmin, xmax);
        } else {
            System.out.println("\nKeine gültige Funktion eingegeben!");
        }
    }
}
