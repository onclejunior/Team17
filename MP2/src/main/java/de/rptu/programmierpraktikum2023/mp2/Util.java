package de.rptu.programmierpraktikum2023.mp2;

import ch.obermuhlner.math.big.BigDecimalMath;
import de.rptu.programmierpraktikum2023.mp2.antlr.FunctionLexer;
import de.rptu.programmierpraktikum2023.mp2.antlr.FunctionParser;
import de.rptu.programmierpraktikum2023.mp2.antlr.FunctionVisitor;
import de.rptu.programmierpraktikum2023.mp2.functions.Function;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTree;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.style.Styler;
import org.knowm.xchart.style.markers.SeriesMarkers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Util {
    public static Function promptFunction(FunctionVisitor<Function> visitor) throws IOException {
        System.out.println("\nf(x) =");
        String input = readLine();
        return parseFunction(input, visitor);
    }

    public static BigDecimal promptXMin() throws IOException {
        return promptBigDecimal("\nxmin =");
    }

    public static BigDecimal promptXMax() throws IOException {
        return promptBigDecimal("\nxmax =");
    }

    public static void plotFunction(Function f, BigDecimal xmin, BigDecimal xmax) {
        XYChart chart = getChart(f, xmin, xmax);
        new SwingWrapper<>(chart).displayChart();
    }

    private static BigDecimal promptBigDecimal(String prompt) throws IOException {
        while (true) {
            System.out.println(prompt);
            try {
                return new BigDecimal(readLine());
            } catch (NumberFormatException e) {
                System.out.println("Not a number!");
            }
        }
    }

    private static String readLine() throws IOException {
        return new BufferedReader(new InputStreamReader(System.in)).readLine();
    }

    public static Function parseFunction(String input, FunctionVisitor<Function> visitor) {
        FunctionLexer lexer = new FunctionLexer(CharStreams.fromString(input));
        lexer.removeErrorListeners();
        lexer.addErrorListener(ThrowingErrorListener.INSTANCE);
        FunctionParser parser = new FunctionParser(new CommonTokenStream(lexer));
        parser.removeErrorListeners();
        parser.addErrorListener(ThrowingErrorListener.INSTANCE);
        ParseTree tree = parser.expr();
        return visitor.visit(tree);
    }

    private static class ThrowingErrorListener extends BaseErrorListener {
        public static final ThrowingErrorListener INSTANCE = new ThrowingErrorListener();

        @Override
        public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e)
                throws ParseCancellationException {
            throw new ParseCancellationException("line " + line + ":" + charPositionInLine + " " + msg);
        }
    }

    public static XYChart getChart(Function f, BigDecimal xmin, BigDecimal xmax) {
        if (xmin.compareTo(xmax) >= 0) {
            throw new IllegalArgumentException("xmax muss echt größer als xmin sein!");
        }

        XYChart chart = new XYChartBuilder().width(800).height(600).title("Mini CAS").xAxisTitle("x").yAxisTitle("y").build();
        chart.getStyler().setLegendPosition(Styler.LegendPosition.OutsideS);
        chart.getStyler().setLocale(Locale.GERMAN);

        // Determine a step size of the form 10^i where i is an integer such that we have between 100 and 999 samples.
        BigDecimal xwidth = xmax.subtract(xmin);
        BigDecimal xstep = new BigDecimal(BigInteger.ONE, 2 - BigDecimalMath.exponent(xwidth));

        // Half the step size until we have at least 500 samples
        while (xwidth.divideToIntegralValue(xstep).intValueExact() < 500) {
            xstep = xstep.divide(new BigDecimal(2), MathContext.UNLIMITED);
        }

        // Data for x-axis
        List<BigDecimal> xdata = getSamples(x -> x, xmin, xmax, xstep);

        // Plot the function itself
        chart.addSeries(
                "f(x) = " + f,
                xdata,
                getSamples(guard(f::apply), xmin, xmax, xstep)
        ).setMarker(SeriesMarkers.NONE);

        // Plot the derivative
        Function df = f.derive();
        if (df != null) {
            // Try to simplify
            Function df_simple = df.simplify();
            if (df_simple == null) {
                df_simple = df;
            }

            chart.addSeries(
                    "f'(x) = " + df_simple,
                    xdata,
                    getSamples(guard(df_simple::apply), xmin, xmax, xstep)
            ).setMarker(SeriesMarkers.NONE);
        } else {
            System.out.println("Fehler: Ableitung von " + f + " ergibt null und kann daher nicht geplottet werden!");
        }
        return chart;
    }

    private static java.util.function.Function<BigDecimal, BigDecimal> guard(java.util.function.Function<BigDecimal, BigDecimal> f) {
        return x -> {
            try {
                return f.apply(x);
            } catch (RuntimeException e) {
                e.printStackTrace();
                return null;
            }
        };
    }

    private static List<BigDecimal> getSamples(java.util.function.Function<BigDecimal, BigDecimal> f, BigDecimal xmin, BigDecimal xmax, BigDecimal xstep) {
        List<BigDecimal> samples = new ArrayList<>(xmax.subtract(xmin).divideToIntegralValue(xstep).intValueExact() + 1);
        int i = 0;
        BigDecimal x;
        for (x = xmin; x.compareTo(xmax) < 0; x = x.add(xstep)) {
            samples.add(f.apply(x));
        }
        samples.add(f.apply(xmax));
        return samples;
    }
}
