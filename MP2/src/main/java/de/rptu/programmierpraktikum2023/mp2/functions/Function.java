package de.rptu.programmierpraktikum2023.mp2.functions;

import java.math.BigDecimal;

public interface Function {
    String toString();
    BigDecimal apply(BigDecimal x);
    Function derive();
    default Function simplify() {
        return this;
    }
}
