package de.rptu.programmierpraktikum2023.mp2.functions;

import java.math.BigDecimal;

public class Div implements Function {

    final Function f1;
    final Function f2;

    public Div(Function f1, Function f2) {
        this.f1 = f1;
        this.f2 = f2;
    }

    @Override
    public String toString() {
        return "(" + f1.toString() + " / " + f2.toString() + ")";
    }

    @Override
    public BigDecimal apply(BigDecimal x) {
        if (f2.apply(x) == BigDecimal.ZERO) {
            return null;
        } else {
            return MathUtils.divide(f1.apply(x), f2.apply(x));
        }
    }

    @Override
    public Function derive() { //bruch x/y -- ableitung ist (x'*y)-(x*y') / y²
        return new Div(new Add(new Mult(f1.derive(), f2), new Mult(new Const(new BigDecimal(-1)), new Mult(f1, f2.derive()))), new Mult(f2, f2));
    }
}
