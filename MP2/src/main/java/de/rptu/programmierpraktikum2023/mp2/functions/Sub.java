package de.rptu.programmierpraktikum2023.mp2.functions;

import java.math.BigDecimal;

public class Sub implements Function {
    Function f1;
    Function f2;

    public Sub(Function f1, Function f2) {
        this.f1 = f1;
        this.f2 = f2;
    }

    @Override
    public String toString() {
        return "( " + this.f1.toString() + " - " + this.f2.toString() + " )";

    }

    @Override
    public BigDecimal apply(BigDecimal x) {
        return MathUtils.subtract(this.f1.apply(x), this.f2.apply(x));
    }

    @Override
    public Function derive() {
        return new Sub(this.f1.derive(), this.f2.derive());
    }

    @Override
    public Function simplify() {
        Function result = new Sub(this.f1, this.f2);

        // f(x) - 0 = f(x),
        if ((this.f1 instanceof Const) && this.f1.apply(BigDecimal.ONE).equals(BigDecimal.ZERO)) {

            result = this.f2;
        }

        return result;
    }
}

