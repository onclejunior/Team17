package de.rptu.programmierpraktikum2023.mp2.functions;

import java.math.BigDecimal;

public class Const implements Function {
    final BigDecimal number;

    public Const(BigDecimal number) {
        this.number = number;
    }


    @Override
    public String toString() {
        return String.valueOf(number);
    }

    @Override
    public BigDecimal apply(BigDecimal x) {
        return number;
    }

    @Override
    public Function derive() {
        return new Const(BigDecimal.ZERO); //ableitung von const ist immmer 0
    }
}
