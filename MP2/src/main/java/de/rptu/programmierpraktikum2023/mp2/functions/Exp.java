package de.rptu.programmierpraktikum2023.mp2.functions;

import java.math.BigDecimal;

public class Exp implements Function{

    final Function f;

    public Exp(Function f) {
        this.f = f;
    }

    @Override
    public String toString() {
        return "exp(" + f.toString() + ")";
    }

    @Override
    public BigDecimal apply(BigDecimal x) {
        return BigDecimal.valueOf(Math.exp(f.apply(x).doubleValue()));
    }

    @Override //ableitung von exp(f) ist ableitung von exponent mal exp: f'*exp
    public Function derive() {
        return new Mult(f.derive(), new Exp(f));
    }
}
