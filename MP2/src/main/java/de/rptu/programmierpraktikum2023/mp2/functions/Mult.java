package de.rptu.programmierpraktikum2023.mp2.functions;

import java.math.BigDecimal;

public class Mult implements Function {

    final Function f1;
    final Function f2;

    public Mult(Function f1, Function f2) {
        this.f1 = f1;
        this.f2 = f2;
    }

    @Override
    public String toString() {
        return "(" + f1.toString() + " * " + f2.toString() + ")";
    }

    @Override
    public BigDecimal apply(BigDecimal x) {
        return MathUtils.multiply(f1.apply(x), f2.apply(x));
    }

    @Override //ableitung von x*y --> (x'*y)+(x*y')
    public Function derive() {
        return new Add(new Mult(f1.derive(), f2), new Mult(f2.derive(), f1));
    }
}
