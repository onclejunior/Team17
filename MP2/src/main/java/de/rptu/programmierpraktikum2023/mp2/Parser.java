package de.rptu.programmierpraktikum2023.mp2;

import de.rptu.programmierpraktikum2023.mp2.antlr.FunctionBaseVisitor;
import de.rptu.programmierpraktikum2023.mp2.antlr.FunctionParser;
import de.rptu.programmierpraktikum2023.mp2.functions.*;


import java.math.BigDecimal;


public class Parser extends FunctionBaseVisitor<Function> {


    //This function is used to put an expression inside the paranthesis
    @Override
    public Function visitParExpr(FunctionParser.ParExprContext ctx) {
        //this is used to add expression inside a parenthesis
        return this.visit(ctx.expr());
    }

    /**
     * If the operator is SIN, return a new Sin object with the expression as the argument. If the operator is COS, return
     * a new Cos object with the expression as the argument
     * A function is returned
     */
    @Override
    public Function visitTrigExp(FunctionParser.TrigExpContext ctx) {
        switch (ctx.op.getType()){
            case (FunctionParser.SIN):
                return new Sin(this.visit(ctx.expr()));
            case (FunctionParser.COS):
                return new Cos(this.visit(ctx.expr()));
            default:
                throw new RuntimeException();
        }
    }



    /**
     * If the operator is EXP, return a new Exp object with the expression as its argument. Otherwise, if the operator is
     * LOG, return a new Log object with the expression as its argument. Otherwise, throw a runtime exception.
     * A function is returned
     */
    @Override
    public Function visitExpExpr(FunctionParser.ExpExprContext ctx) {
        switch (ctx.op.getType()){
            case (FunctionParser.EXP):
                return new Exp(this.visit(ctx.expr()));
            case (FunctionParser.LOG):
                return new Log(this.visit(ctx.expr()));
            default:
                throw new RuntimeException();
        }
    }

    @Override
    // my idea here was/is, if the sign is minus or plus, I will have to multiply the context with a negative or positive sign respectively
    public Function visitSgnValExpr(FunctionParser.SgnValExprContext ctx) {
        // return ctx.sgn != ctx.PLUS() ?  new Const(new BigDecimal(-1)) : new Const(BigDecimal.ONE);
        if (ctx.sgn != null && (ctx.sgn.getType() == FunctionParser.MINUS)){
            if (ctx.var() instanceof FunctionParser.ConstVarContext ){
                return new Mult(new Const(new BigDecimal(-1)), new Const(new BigDecimal((ctx.var().getText()))));
            }else {
                return new Mult(new Const(new BigDecimal(-1)),new X());
            }
        } else {
            if (ctx.var() instanceof FunctionParser.ConstVarContext ){
                return new Mult(new Const(BigDecimal.ONE), new Const(new BigDecimal((ctx.var().getText()))));
            }
            return new X();

        }
    }



    /**
     * > If the operator is a plus, return an Add object with the left and right expressions as arguments. If the operator
     * is a minus, return a Sub object with the left and right expressions as arguments
     *  A function is returned
     */
    @Override
    public Function visitAddExpr(FunctionParser.AddExprContext ctx) {
        switch (ctx.op.getType()){
            case (FunctionParser.PLUS):
                return new Add(this.visit(ctx.lexpr), this.visit(ctx.rexpr));
            case (FunctionParser.MINUS):
                return new Sub(this.visit(ctx.lexpr), this.visit(ctx.rexpr));
            default:
                throw new RuntimeException();
        }
    }

    /**
     * > If the operator is a multiplication, return a new multiplication function with the left and right expressions as
     * arguments. Otherwise, if the operator is a division, return a new division function with the left and right
     * expressions as arguments
     * A function is returned
     */
    @Override
    public Function visitMultExpr(FunctionParser.MultExprContext ctx) {
        switch (ctx.op.getType()){
            case (FunctionParser.MULT):
                return new Mult(this.visit(ctx.lexpr), this.visit(ctx.rexpr));
            case (FunctionParser.DIV):
                return  new Div(this.visit(ctx.lexpr), this.visit(ctx.rexpr));
            default:
                throw new RuntimeException();
        }
    }


    // The function `visitConstVar` is called when the parser encounters a constant
    @Override
    public Function visitConstVar(FunctionParser.ConstVarContext ctx) {
        return new Const( new BigDecimal ((ctx.getText())));
    }

    //When the parser encounters an identifier, it creates a new X object and returns it.

    @Override
    public Function visitIdVar(FunctionParser.IdVarContext ctx) {
        return new X();
    }
}


