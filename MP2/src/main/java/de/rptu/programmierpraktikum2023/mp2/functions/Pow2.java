package de.rptu.programmierpraktikum2023.mp2.functions;

import java.math.BigDecimal;

public class Pow2 implements Function {
    Function f1;
    Function f2;

    public Pow2(Function f1, Function f2) {
        this.f1 = f1;
        this.f2 = f2;
    }

    @Override
    public String toString() {
        return "( " + this.f1.toString() + " ^ " + this.f2.toString() + " )";

    }

    @Override
    public BigDecimal apply(BigDecimal x) {
        /*
            since when you apply anything to a constant is does not change I
            apply x to both f1 and f2 because I don't know which one is the variable
            "x"
         */

        return MathUtils.pow(this.f1.apply(x), this.f2.apply(x));
    }

    @Override
    public Function derive() {
        /*
               Derivative of x^3 -> 3x^2
               Basically when f1 is an instance of class X and
               f2 an instance of Const.

               x^n -> n*x^(n-1)
         */
        if (this.f1 instanceof X && this.f2 instanceof Const) {
            //n-1
            Function n_1 = new Sub(this.f2, new Const(BigDecimal.ONE));

            //x^(n-1)
            Function x = new Pow2(this.f1, n_1);

            //n*x^(n-1)
            return new Mult(this.f2, x);
        } else  {

            //rule: x^n -> n' * x^(n) * ln(x)

            // x^(n')
            Function x2 = new Pow2(this.f1, this.f2);

            // n' * x^(n)
            Function x3 = new Mult(this.f2.derive(), x2);

            //x^n -> n' * x^(n) * ln(x)
            return new Mult(x3, new Log(this.f1));
        }

    }
}


