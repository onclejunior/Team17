package de.rptu.programmierpraktikum2023.mp2.functions;

import ch.obermuhlner.math.big.BigDecimalMath;

import java.math.BigDecimal;
import java.math.MathContext;

public class MathUtils {
    private static final MathContext MATH_CONTEXT = new MathContext(100);

    public static BigDecimal add(BigDecimal summand1, BigDecimal summand2) {
        return summand1.add(summand2);
    }

    public static BigDecimal subtract(BigDecimal minuend, BigDecimal subtrahend) {
        return minuend.subtract(subtrahend);
    }

    public static BigDecimal multiply(BigDecimal multiplicand1, BigDecimal multiplicand2) {
        return multiplicand1.multiply(multiplicand2);
    }

    public static BigDecimal divide(BigDecimal dividend, BigDecimal divisor) {
        return dividend.divide(divisor, MATH_CONTEXT);
    }

    public static BigDecimal pow(BigDecimal base, BigDecimal exponent) {
        try {
            return BigDecimalMath.pow(base, exponent.longValueExact(), MathContext.UNLIMITED);
        } catch (ArithmeticException e) {
            return BigDecimalMath.pow(base, exponent, MATH_CONTEXT);
        }
    }

    public static BigDecimal exp(BigDecimal value) {
        return BigDecimalMath.exp(value, MATH_CONTEXT);
    }

    public static BigDecimal log(BigDecimal value) {
        return BigDecimalMath.log(value, MATH_CONTEXT);
    }

    public static BigDecimal sin(BigDecimal value) {
        return BigDecimalMath.sin(value, MATH_CONTEXT);
    }

    public static BigDecimal cos(BigDecimal value) {
        return BigDecimalMath.cos(value, MATH_CONTEXT);
    }
}
