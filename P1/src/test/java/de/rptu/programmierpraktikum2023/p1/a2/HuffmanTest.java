package de.rptu.programmierpraktikum2023.p1.a2;

import de.rptu.programmierpraktikum2023.p1.util.TestBitInputStream;
import de.rptu.programmierpraktikum2023.p1.util.TestBitOutputStream;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class HuffmanTest {
    @Test
    void testExample1() {
        // Generate a frequency table
        long[] frequencies = new long[256];
        frequencies['a'] = 7;
        frequencies['b'] = 5;
        frequencies['c'] = 4;

        // Build the Huffman tree
        Huffman huffman = HuffmanFactory.getHuffman();
        Node rootNode = huffman.buildTree(frequencies);

        Node expected = new Node( new Node('a') , new Node(new Node('c') , new Node('b')));
        assertEquals(expected, rootNode);

    }

    @Test
    void testExample2() throws IOException {
        // Example tree
        Node rootNode = new Node(new Node('a'), new Node(new Node('b'), new Node('c')));

        // Encode some data
        Huffman huffman = HuffmanFactory.getHuffman();
        Encoder encoder = huffman.buildEncoder(rootNode);
        TestBitOutputStream out = new TestBitOutputStream(1024);
        encoder.writeData('a', out);
        assertEquals(1, out.getWrittenBits()); // 'a' requires one bit
        encoder.writeData('b', out);
        assertEquals(3, out.getWrittenBits()); // 'b' requires two bits
        encoder.writeData('c', out);
        assertEquals(5, out.getWrittenBits()); // 'c' requires two bits

        // Decode the data again
        Decoder decoder = huffman.buildDecoder(rootNode);
        TestBitInputStream in = new TestBitInputStream(out.getWrittenBuffer());
        assertEquals('a', decoder.readData(in));
        assertEquals('b', decoder.readData(in));
        assertEquals('c', decoder.readData(in));
        assertEquals(5, in.getConsumedBits());
    }

    @Test
    void testExample3() throws IOException {
        // Example tree
        Node rootNode = new Node(new Node('a'), new Node(new Node('b'), new Node('c')));

        // Write the tree
        TestBitOutputStream out = new TestBitOutputStream(1024);
        Huffman huffman = HuffmanFactory.getHuffman();
        huffman.writeNode(rootNode, out);

        // Read the tree and check it is equal to the original
        TestBitInputStream in = new TestBitInputStream(out.getWrittenBuffer());
        Node restoredNode = huffman.readNode(in);
        assertEquals(rootNode, restoredNode);
    }
}
