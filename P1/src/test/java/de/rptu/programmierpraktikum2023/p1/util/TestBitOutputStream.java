package de.rptu.programmierpraktikum2023.p1.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class TestBitOutputStream implements BitOutputStream {
    private final ByteArrayOutputStream out;
    private final BitOutputStreamImpl delegate;

    /**
     * Construct a {@link BitOutputStream} that writes data to an internal buffer.
     *
     * @param capacity the capacity the internal buffer should have, in bytes
     */
    public TestBitOutputStream(int capacity) {
        out = new ByteArrayOutputStream(capacity);
        delegate = new BitOutputStreamImpl(out);
    }

    @Override
    public void writeBit(boolean bit) throws IOException {
        delegate.writeBit(bit);
    }

    @Override
    public void writeByte(int data) throws IOException {
        delegate.writeByte(data);
    }

    /**
     * Get the number of bits that have been written to this stream.
     *
     * @return number of written bits
     */
    public long getWrittenBits() {
        return delegate.getWrittenBits();
    }

    /**
     * Get the data that has been written to this stream. The result can be used to construct a
     * {@link TestBitInputStream}.
     * <p>
     * The operation closes this stream if the number of written bits is not a multiple of 8, because then it adds some
     * bits as padding to fill the next byte.
     *
     * @return data that has been written to this stream
     */
    public byte[] getWrittenBuffer() throws IOException {
        delegate.flush();
        return out.toByteArray();
    }
}
