package de.rptu.programmierpraktikum2023.p1.a1;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.LinkedList;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class PriorityQueueTest {
    /**
     * Diese Methode wird verwendet, um Instanzen von PriorityQueue Implementierungen an Testmethoden zu übergeben.
     */
    public static List<PriorityQueue<String>> getPriorityQueueInstances() {
        List<PriorityQueue<String>> implementations = new LinkedList<>();
        // Um Compilefehler zu vermeiden, sind die Instanziierungen der PriorityQueue Implementierungen auskommentiert.
        // Kommentieren Sie die Zeilen ein, sobald Sie die entsprechenden Klassen implementiert haben.
        implementations.add(new ListQueue<>());
        implementations.add(new SkewHeap<>());
        return implementations;
    }

    @ParameterizedTest
    @MethodSource("getPriorityQueueInstances")
    public void testExample(PriorityQueue<String> queue) {
        System.out.println("Teste testExample mit " + queue.getClass().getSimpleName());

        // Test: eine frisch initialisierte Prioritätswarteschlange ist leer
        assertTrue(queue.isEmpty());
        System.out.println("queue.isEmpty()");


        queue.insert(4711, "Foo");
        System.out.println("queue.insert(4711, Foo)");

        // Fügen Sie hier weitere Checks ein.

        // Test: to check if Peek returns an entry if there is only one Entry in the queue
        try {
            assertEquals(4711, queue.peek().priority);
            assertEquals("Foo", queue.peek().element);
            System.out.println("Wenn die Priorität Warteschlange nur ein Eintrag enthält, sollte die 'peek' Methode"+
                    " Entry:  - passed"+ "zurückgeben");

        } catch (AssertionError e) {
            System.out.println("Es wurde ein Eintrag mit Priorität: 4711 und Element: 'Foo' erwartet. \n"
                    + "Aber es wurde ein Eintrag mit Priority:" + queue.peek().priority +  "und Element:"
                    + queue.peek().element +"erhalten");
            throw e;
        }
        System.out.println();
    }

    @ParameterizedTest
    @MethodSource("getPriorityQueueInstances")
    public void priorityQueueBeispiel2(PriorityQueue<String> queue) {
        System.out.println("Teste priorityQueueBeispiel mit " + queue.getClass().getSimpleName());

        // Test: eine frisch initialisierte Prioritätswarteschlange ist leer
        assertTrue(queue.isEmpty());
        System.out.println("queue.isEmpty()");


        queue.insert(4711, "Foo");
        System.out.println("queue.insert(4711, Foo)");

        // Fügen Sie hier weitere Checks ein.

        // Test: Überprüfe, ob die Methode extractMin funktioniert
        Entry<String> extract = queue.extractMin();
        try {

            assertEquals(4711, extract.priority);
            assertEquals("Foo", extract.element);
            System.out.println("extractMin funktioniert : - passed");
        } catch (AssertionError e) {
            System.out.println("extractMin funktioniert : - failed \n"+
                    "Es wurde ein Eintrag mit Priorität: 4711 und Element: 'Foo' erwartet. \n"
                            + "Aber es wurde ein Eintrag mit Priority:" + queue.peek().priority +  "und Element:"
                            + queue.peek().element +"erhalten");
            throw e;
        }
        System.out.println();

        // Test: Wenn die Prioritätswarteschlange nur ein Eintrag enthält und wir extractMin, dann muss die die queue leer werden.
        try {
            assertTrue(queue.isEmpty());
            System.out.println(" Wenn die Prioritätswarteschlange nur ein Eintrag enthält und wir extractMin, dann muss die die queue leer werden: - passed");
        } catch (AssertionError e) {
            System.out.println(" Wenn die Prioritätswarteschlange nur ein Eintrag enthält und wir extractMin, dann muss die die queue leer werden: - failed");
            throw e;
        }
        System.out.println();
    }


    @ParameterizedTest
    @MethodSource("getPriorityQueueInstances")
    public void priorityQueueBeispiel3(PriorityQueue<String> queue) {
        System.out.println("Teste priorityQueueBeispiel mit " + queue.getClass().getSimpleName());

        // Test: eine frisch initialisierte Prioritätswarteschlange ist leer
        assertTrue(queue.isEmpty());
        System.out.println("queue.isEmpty()");

        // Test: Mehr als ein Eintrag einfügen um zu überprüfen, ob die Methode insert funktioniert

        queue.insert(5279, " Tim ");
        queue.insert(4711, " Elias ");
        queue.insert(19, " Junior ");
        queue.insert(3340, " Jens ");
        queue.insert(271, " Frazier ");

        System.out.println("queue.insert(5279, \" Tim \");");
        System.out.println("queue.insert(4711, \" Elias \");");
        System.out.println("queue.insert(19, \" Junior \");");
        System.out.println("queue.insert(3340, \" Jens \");");
        System.out.println("queue.insert(271, \" Frazier \");");


        // Fügen Sie hier weitere Checks ein.
        // Test: Peek method
        try {
            assertEquals( 19,queue.peek().priority);
            assertEquals( " Junior ",queue.peek().element);
            System.out.println(" Mehr als ein Eintrag einfügen (peek Methode):  - passed");

        } catch (AssertionError e) {
            System.out.println(" Mehr als ein Eintrag einfügen (peek method):  - failed \n"
                    + "Es wurde ein Eintrag mit Priorität: 19 und Element: 'Junior' erwartet. \n"
                            + "Aber es wurde ein Eintrag mit Priority:" + queue.peek().priority +  "und Element:"
                            + queue.peek().element +"erhalten");
            throw e;
        }
        System.out.println();
    }


    @ParameterizedTest
    @MethodSource("getPriorityQueueInstances")
    public void priorityQueueBeispiel4(PriorityQueue<String> queue) {
        System.out.println("Teste priorityQueueBeispiel mit " + queue.getClass().getSimpleName());

        // Test: eine frisch initialisierte Prioritätswarteschlange ist leer
        assertTrue(queue.isEmpty());
        System.out.println("queue.isEmpty()");

        // Test: Mehr als ein Eintrag einfügen um zu überprüfen, ob die Methode insert funktioniert

        queue.insert(5279, " Tim ");
        queue.insert(4711, " Elias ");
        queue.insert(19, " Junior ");
        queue.insert(3340, " Jens ");
        queue.insert(271, " Frazier ");

        System.out.println("queue.insert(5279, \" Tim \");");
        System.out.println("queue.insert(4711, \" Elias \");");
        System.out.println("queue.insert(19, \" Junior \");");
        System.out.println("queue.insert(3340, \" Jens \");");
        System.out.println("queue.insert(271, \" Frazier \");");

        // Fügen Sie hier weitere Checks ein.
        // Test: ExtractMin method
        Entry<String> extract2 = queue.extractMin();
        try {
            assertEquals(19,extract2.priority);
            assertEquals(" Junior ", extract2.element );
            System.out.println(" Mehr als ein Eintrag einfügen (peek Methode):  - passed");

        } catch (AssertionError e) {
            System.out.println(" Mehr als ein Eintrag einfügen (peek method):  - failed \n"
                    + "Es wurde ein Eintrag mit Priorität: 19 und Element: 'Junior' erwartet. \n"
                    + "Aber es wurde ein Eintrag mit Priority:" + extract2.priority +  "und Element:"
                    + extract2.element +"erhalten");
            throw e;

        }
        System.out.println();

        // Fügen Sie hier weitere Checks ein.
        // Test: test the Peek method after calling extractMin
        try {
            assertEquals( 271,queue.peek().priority);
            assertEquals( " Frazier ",queue.peek().element);
            System.out.println(" teste die Peek-Methode nach dem Aufruf von extractMin:  - passed");

        } catch (AssertionError e) {
            System.out.println("teste die Peek-Methode nach dem Aufruf von extractMin:  - failed \n"
                    + "Es wurde ein Eintrag mit Priorität: 271 und Element: 'Frazier' erwartet. \n"
                    + "Aber es wurde ein Eintrag mit Priority:" + queue.peek().priority +  "und Element:"
                    +  queue.peek().element +"erhalten");
            throw e;
        }
        System.out.println();
    }

    @ParameterizedTest
    @MethodSource("getPriorityQueueInstances")
    public void priorityQueueBeispiel5(PriorityQueue<String> queue) {
        System.out.println("Teste priorityQueueBeispiel mit " + queue.getClass().getSimpleName());

        // Test: eine frisch initialisierte Prioritätswarteschlange ist leer
        assertTrue(queue.isEmpty());
        System.out.println("queue.isEmpty()");

        // Test: Mehr als ein Eintrag einfügen um zu überprüfen, ob die Methode insert funktioniert

        queue.insert(5279, " Tim ");
        queue.insert(4711, " Elias ");
        queue.insert(19, " Junior ");
        queue.insert(3340, " Jens ");
        queue.insert(271, " Frazier ");

        System.out.println("queue.insert(5279, \" Tim \");");
        System.out.println("queue.insert(4711, \" Elias \");");
        System.out.println("queue.insert(19, \" Junior \");");
        System.out.println("queue.insert(3340, \" Jens \");");
        System.out.println("queue.insert(271, \" Frazier \");");

        // Fügen Sie hier weitere Checks ein.

        // Test: Empty method
        try {
            assertFalse(queue.isEmpty());
            System.out.println(" Mehr als ein Eintrag einfügen (isEmpty Methode): - passed");
        } catch (AssertionError e) {
            System.out.println(" Mehr als ein Eintrag einfügen (isEmpty Methode): - failed");
            throw e;
        }
        System.out.println();
    }

    @ParameterizedTest
    @MethodSource("getPriorityQueueInstances")
    public void priorityQueueBeispiel6(PriorityQueue<String> queue) {
        System.out.println("Teste priorityQueueBeispiel mit " + queue.getClass().getSimpleName());

        // Test: eine frisch initialisierte Prioritätswarteschlange ist leer
        assertTrue(queue.isEmpty());
        System.out.println("queue.isEmpty()");


        // Fügen Sie hier weitere Checks ein.

        // Test:  Wenn die queue leer ist, solte peek null zurückgeben und keine ausnahme auslösen
        try {
            assertNull(queue.peek());
            System.out.println(" Wenn die queue leer ist, solte peek null zurückgeben und keine ausnahme auslösen: - passed");
        } catch (AssertionError e) {
            System.out.println(" Wenn die queue leer ist, solte peek null zurückgeben und keine ausnahme auslösen: - failed");
            throw e;
        }
        System.out.println();
    }


    @ParameterizedTest
    @MethodSource("getPriorityQueueInstances")
    public void priorityQueueBeispiel7(PriorityQueue<String> queue) {
        System.out.println("Teste priorityQueueBeispiel mit " + queue.getClass().getSimpleName());

        // Test: eine frisch initialisierte Prioritätswarteschlange ist leer
        assertTrue(queue.isEmpty());
        System.out.println("queue.isEmpty()");


        // Test:  Wenn die queue leer ist, solte peek null zurückgeben und keine ausnahme auslösen
        try {
            assertNull(queue.peek());
            System.out.println(" Wenn die queue leer ist, solte peek null zurückgeben und keine ausnahme auslösen: - passed");
        } catch (AssertionError e) {
            System.out.println(" Wenn die queue leer ist, solte peek null zurückgeben und keine ausnahme auslösen: - failed");
            throw e;
        }
        System.out.println();

        // wir fuegen 10 Einträge in die queue

        // Eine Liste von Elemente und Prioritäten
        List<String> elements = new ArrayList<>();
        List<Long> priorities = new ArrayList<>();

        // Wir benutzen diese zwei Listen um ein Random String zu erzeugen
        List<String> firstName = Arrays.asList(" Bruno ", " Marcus ", " Christian ", " Raphael ", " Alejandro ", " Diogo ", " Victor ");
        List<String> lastName = Arrays.asList(" Fernandes ", " Rashford ", " Ericksen ", " Varane ", " Garnacho ", " Dalot ", " Lindelof ");
        Random rand = new Random();

        for (int i = 0; i < 14; i++) {
            // Erzeugung eines Random long
            long rand_long2 = (long)(rand.nextInt()*(4000/Math.pow(2,32)));
            long rand_long = (long)(rand.nextDouble()*4000);

            // Erzeugung eines Random String
            String rand_String = firstName.get(rand.nextInt(7)) + lastName.get(rand.nextInt(7));

            // Wir fügen sie in die entsprechenden Listen
            priorities.add(rand_long);
            elements.add(rand_String);

            // Wir fügen die Random long und String in die queue
            queue.insert(rand_long, rand_String);
            System.out.println("queue.insert(" + rand_long + ","+ rand_String +" )");
        }

        //Abrufen der höchsten und niedrigsten Priorität aus der Prioritätsliste
        long highestPriority = priorities.get(0);
        for (int j = 1; j < 14; j++) {
            if (highestPriority > priorities.get(j)) {
                highestPriority = priorities.get(j);
            }
        }

        // Index der höchsten Priorität
        int index = priorities.indexOf(highestPriority);

        // Test: Peek Methode
        try {
            assertEquals(highestPriority,queue.peek().priority);
            assertEquals(elements.get(index),queue.peek().element);
            System.out.println("10 Einträge einfügen (Peek method):  - passed");
        } catch (AssertionError e) {
            System.out.println(" 10 Einträge einfügen  (Peek method):  - failed \n"
                    + "Es wurde ein Eintrag mit Priorität:"+ highestPriority + "  und Element:"+ elements.get(index) +"\n"
                    + "Aber es wurde ein Eintrag mit Priority:" + queue.peek().priority +  "und Element:"
                    +  queue.peek().element +"erhalten");
            throw e;
        }
        System.out.println();

        // Test: ExtractMin Methode
        Entry<String> extract = queue.extractMin();
        try {
            assertEquals(highestPriority,extract.priority);
            assertEquals(elements.get(index), extract.element);
            System.out.println(" 10 Einträge einfügen (ExtractMin method):  - passed");
        } catch (AssertionError e) {
            System.out.println(" 10 Einträge einfügen  (ExtractMin Methode):  - failed \n"
                    + "Es wurde ein Eintrag mit Priorität:"+ highestPriority + "  und Element:"+ elements.get(index) +"\n"
                    + "Aber es wurde ein Eintrag mit Priority:"+ extract.priority +  "und Element:"
                    +  extract.element +"erhalten");
            throw e;
        }
        System.out.println();
    }

    @ParameterizedTest
    @MethodSource("getPriorityQueueInstances")
    public void priorityQueueBeispiel8(PriorityQueue<String> queue) {
        System.out.println("Teste priorityQueueBeispiel mit " + queue.getClass().getSimpleName());

        // Test: eine frisch initialisierte Prioritätswarteschlange ist leer
        assertTrue(queue.isEmpty());
        System.out.println("queue.isEmpty()");


        // Test:  Wenn die queue leer ist, solte peek null zurückgeben und keine ausnahme auslösen
        try {
            assertNull(queue.peek());
            System.out.println(" Wenn die queue leer ist, solte peek null zurückgeben und keine ausnahme auslösen: - passed");
        } catch (AssertionError e) {
            System.out.println(" Wenn die queue leer ist, solte peek null zurückgeben und keine ausnahme auslösen: - failed");
            throw e;
        }
        System.out.println();

        // Wir fügen 100 Einträge in die queue

        // Erzeugung eine liste von Elementen und eine Liste von Prioritäten
        List<String> elements = new ArrayList<>();
        List<Long> priorities = new ArrayList<>();

        // die zwei Listen von String helfen um ein randon String zu erzeugen
        List<String> firstName = Arrays.asList( " Bruno ", " Marcus ", " Christian ", " Raphael ", " Alejandro ", " Diogo ", " Victor ");
        List<String> middleName = Arrays.asList (" Fernandes ", " Rashford ", " Ericksen ", " Varane ", " Garnacho ", " Dalot ", " Lindelof ");
        List<String> LastName = Arrays.asList(" Ruediger ", " Nacho ", " Kroos ", " Modric ", " Valverde ", " Rodrygo ", " Vini ");
        Random rand = new Random();

        for (int i = 0; i < 100; i++) {
            // Erzeugung eines Random long
            //long rand_long2 = rand.nextLong(4000L);
            long rand_long = (long)(rand.nextDouble()*4000);

            // Erzeugung eines Random String
            String rand_String = firstName.get(rand.nextInt(7)) + middleName.get(rand.nextInt(7)) + LastName.get(rand.nextInt(7));

            // Wir fügen sie in die entsprechenden Listen
            priorities.add(rand_long);
            elements.add(rand_String);

            // Wir fügen die Random long und String in die queue
            queue.insert(rand_long, rand_String);
            System.out.println("queue.insert(" + rand_long + ","+ rand_String +" )");

        }


        //Abrufen der höchsten und niedrigsten Priorität aus der Prioritätsliste
        long highestPriority = priorities.get(0);
        for (int j = 1; j < 100; j++) {
            if (highestPriority > priorities.get(j)) {
                highestPriority = priorities.get(j);
            }
        }

        // Index der höchsten Priorit
        int index = priorities.indexOf(highestPriority);

        // Test: Peek Methode
        try {
            assertEquals(highestPriority,queue.peek().priority);
            assertEquals(elements.get(index),queue.peek().element);
            System.out.println("Wir fügen 100 Einträge ein (Peek Methode):  - passed");
        } catch (AssertionError e) {
            System.out.println("  Wir fügen 100 Einträge ein (Peek Methode):  - failed \n"
                    + "Es wurde ein Eintrag mit Priorität:"+ highestPriority + "  und Element:"+ elements.get(index) +"\n"
                    + "Aber es wurde ein Eintrag mit Priority:"+ queue.peek().priority +  "und Element:"
                    +  queue.peek().element +"erhalten");
            throw e;
        }
        System.out.println();

        // Test: ExtractMin method
        Entry<String> extract = queue.extractMin();
        try {
            assertEquals(highestPriority,extract.priority);
            assertEquals(elements.get(index), extract.element);
            System.out.println("inserting 100 Entries (ExtractMin method):  - passed");
        } catch (AssertionError e) {
            System.out.println("  Wir fügen 100 Einträge ein (Peek Methode):  - failed \n"
                    + "Es wurde ein Eintrag mit Priorität:"+ highestPriority + "  und Element:"+ elements.get(index) +"\n"
                    + "Aber es wurde ein Eintrag mit Priority:"+ extract.priority +  "und Element:"
                    +  extract.element +"erhalten");
            throw e;
        }
        System.out.println();
    }
}
