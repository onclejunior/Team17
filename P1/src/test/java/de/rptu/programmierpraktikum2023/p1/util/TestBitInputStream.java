package de.rptu.programmierpraktikum2023.p1.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;

public class TestBitInputStream implements BitInputStream {
    private final BitInputStreamImpl delegate;
    private long consumedBits;

    /**
     * Construct a {@link BitInputStream} that reads data from a given {@code byte[]} buffer.
     *
     * @param buffer the buffer to read from
     */
    public TestBitInputStream(byte[] buffer) {
        delegate = new BitInputStreamImpl(new ByteArrayInputStream(buffer));
    }

    @Override
    public boolean readBit() throws IOException {
        boolean bit = delegate.readBit();
        consumedBits++;
        return bit;
    }

    @Override
    public int readByte() throws IOException {
        int data = delegate.readByte();
        consumedBits += 8;
        return data;
    }

    /**
     * Get the number of bits that have been consumed from this stream.
     *
     * @return number of consumed bits
     */
    public long getConsumedBits() {
        return consumedBits;
    }
}
