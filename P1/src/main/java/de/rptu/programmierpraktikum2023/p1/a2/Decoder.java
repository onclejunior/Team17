package de.rptu.programmierpraktikum2023.p1.a2;

import de.rptu.programmierpraktikum2023.p1.util.BitInputStream;

import java.io.IOException;

public interface Decoder {
    int readData(BitInputStream in) throws IOException;
}
