package de.rptu.programmierpraktikum2023.p1.util;

import de.rptu.programmierpraktikum2023.p1.a2.Decoder;
import de.rptu.programmierpraktikum2023.p1.a2.Encoder;
import de.rptu.programmierpraktikum2023.p1.a2.Huffman;
import de.rptu.programmierpraktikum2023.p1.a2.Node;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class FileOperations {
    public static void compressFile(File inputFile, File outputFile, Huffman huffman) throws IOException {
        // Read the input file, thereby counting how often each byte occurs
        System.out.println("Reading file " + inputFile + "...");
        long[] frequencies = new long[256];
        try (FileInputStream fileInputStream = new FileInputStream(inputFile);
             BufferedInputStream input = new BufferedInputStream(fileInputStream)
        ) {
            int inputByte;
            while ((inputByte = input.read()) != -1) {
                frequencies[inputByte]++;
            }
        }

        // Check that file is non-empty
        long inputFileSize = Arrays.stream(frequencies).sum();
        if (inputFileSize == 0) {
            throw new IllegalArgumentException("Cannot compress an empty file!");
        }
        System.out.println("The file has " + inputFileSize + " bytes." + System.lineSeparator());

        // Print frequency table (for debugging)
        System.out.println("Frequency table:");
        for (int i = 0; i < frequencies.length; i++) {
            if (frequencies[i] > 0) {
                String displayChar = i == '\n' ? "\\n"
                        : i == '\r' ? "\\r"
                        : i == '\t' ? "\\t"
                        : i >= 32 && i <= 126 ? String.valueOf((char) i) : null;
                System.out.println(i + (displayChar == null ? "" : " ('" + displayChar + "')") + ": " + frequencies[i]);
            }
        }
        System.out.println();

        // Build the Huffman tree and encoder
        Node rootNode = huffman.buildTree(frequencies);
        System.out.println("Huffman tree:" + System.lineSeparator() + rootNode + System.lineSeparator());
        Encoder encoder = huffman.buildEncoder(rootNode);

        // Read the input file again, thereby encoding each byte and writing it to the output file
        System.out.println("Writing file " + outputFile + "...");
        try (FileInputStream fileInputStream = new FileInputStream(inputFile);
             BufferedInputStream input = new BufferedInputStream(fileInputStream);
             FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
             BufferedOutputStream output = new BufferedOutputStream(fileOutputStream)
        ) {
            // The encoded file first contains the size of the decoded file such that the decompression algorithm knows
            // when to stop reading bits (the compressed file might contain trailing zero bits to fill a byte).
            output.write(ByteBuffer.allocate(Long.BYTES).putLong(inputFileSize).array());
            long fileSizeBits = 8 * Long.BYTES;
            System.out.println("Used " + showBitsAndBytes(fileSizeBits) + " to store the file size.");

            try (BitOutputStreamImpl bitOutputStream = new BitOutputStreamImpl(output)) {
                // Write the Huffman tree
                huffman.writeNode(rootNode, bitOutputStream);
                long huffmanTreeBits = bitOutputStream.getWrittenBits();
                System.out.println("Used " + showBitsAndBytes(huffmanTreeBits) + " to store the Huffman tree.");

                // Encode each byte and directly write the result to the output file.
                long encodedBytes = 0;
                int inputByte;
                while ((inputByte = input.read()) != -1) {
                    encoder.writeData(inputByte, bitOutputStream);
                    encodedBytes++;
                }
                long contentBits = bitOutputStream.getWrittenBits() - huffmanTreeBits;
                System.out.println("Used " + showBitsAndBytes(contentBits) + " to store encoding for " + encodedBytes + " bytes from input file." + System.lineSeparator());

                // Check file size is still the same
                if (encodedBytes != inputFileSize) {
                    throw new IllegalStateException("Input file size has changed!");
                }

                // Show statistics
                long totalBits = fileSizeBits + huffmanTreeBits + contentBits;
                long totalBytes = (totalBits + 7) / 8;
                System.out.println("Total output file size: " + showBitsAndBytes(totalBits));
                double compressRatio = 100 * (1 - (double) totalBytes / (double) inputFileSize);
                System.out.println("Compression ratio: 1 - " + totalBytes + "/" + inputFileSize + " = " + String.format("%.2f", compressRatio) + "%");
            }
        }

        System.out.println(System.lineSeparator() + "Done.");
    }

    public static void decompressFile(File inputFile, File outputFile, Huffman huffman) throws IOException {
        System.out.println("Reading file " + inputFile + "...");
        try (FileInputStream fileInputStream = new FileInputStream(inputFile);
             BufferedInputStream input = new BufferedInputStream(fileInputStream)
        ) {
            // First read a long value, denoting the number of bytes the decoded file has.
            byte[] fileSizeBuffer = new byte[Long.BYTES];
            if (input.read(fileSizeBuffer) != fileSizeBuffer.length) {
                throw new IOException("Unexpected end of stream!");
            }
            long bytesToDecode = ByteBuffer.wrap(fileSizeBuffer).getLong();
            System.out.println("Decoded length will be " + bytesToDecode + " bytes." + System.lineSeparator());

            // Restore Huffman tree and decoder
            BitInputStreamImpl bitInputStream = new BitInputStreamImpl(input);
            Node rootNode = huffman.readNode(bitInputStream);
            System.out.println("Huffman tree:" + System.lineSeparator() + rootNode + System.lineSeparator());
            Decoder decoder = huffman.buildDecoder(rootNode);

            // Decode the input and directly write the decoded bytes to the output file.
            System.out.println("Writing file " + outputFile + "...");
            try (FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
                 BufferedOutputStream output = new BufferedOutputStream(fileOutputStream)
            ) {
                while (bytesToDecode > 0) {
                    output.write(decoder.readData(bitInputStream));
                    bytesToDecode--;
                }
            }
        }

        System.out.println(System.lineSeparator() + "Done.");
    }

    private static String showBitsAndBytes(long bits) {
        return bits + " bits (" + ((bits + 7) / 8) + " bytes)";
    }
}
