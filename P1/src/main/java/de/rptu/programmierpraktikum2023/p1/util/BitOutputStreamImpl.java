package de.rptu.programmierpraktikum2023.p1.util;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.OutputStream;

class BitOutputStreamImpl implements BitOutputStream, Closeable, Flushable {
    private final OutputStream out;
    private final boolean[] buffer = new boolean[8];
    private byte bufferPos = 0;
    private boolean closed = false;
    private long writtenBits = 0;


    public BitOutputStreamImpl(OutputStream out) {
        this.out = out;
    }

    public void writeBit(boolean bit) throws IOException {
        checkNotClosed();
        doWriteBit(bit);
    }

    public void writeByte(int data) throws IOException {
        checkNotClosed();
        if (data < 0 || data > 255) {
            throw new IllegalArgumentException("Provided byte must be between 0 and 255, but was " + data);
        }
        for (int i = 0; i < 8; i++) {
            doWriteBit((data & 1 << 7 - i) != 0);
        }
    }

    /**
     * Flushes this stream by writing any buffered output to the underlying stream.
     * <p>
     * Note that the underlying stream can only write full bytes. Therefore, if the buffer contains more than one but
     * less than eight bits, then we append zero bits as necessary. If that happens, then this stream is marked as
     * closed, preventing further writes, but without closing the underlying stream.
     */
    @Override
    public void flush() throws IOException {
        if (bufferPos != 0) {
            doFlush();
            closed = true;
        }
        out.flush();
    }

    @Override
    public void close() throws IOException {
        flush();
        closed = true;
        out.close();
    }

    public long getWrittenBits() {
        return writtenBits;
    }

    private void checkNotClosed() throws IOException {
        if (closed) {
            throw new IOException("BitOutputStream is closed!");
        }
    }


    private void doWriteBit(boolean bit) throws IOException {
        buffer[bufferPos++] = bit;
        writtenBits++;
        if (bufferPos == 8) {
            doFlush();
        }
    }

    private void doFlush() throws IOException {
        int x = 0;
        for (int i = 0; i < bufferPos; i++) {
            if (buffer[i]) {
                x |= 1 << 7 - i;
            }
        }
        out.write(x);
        bufferPos = 0;
    }
}
