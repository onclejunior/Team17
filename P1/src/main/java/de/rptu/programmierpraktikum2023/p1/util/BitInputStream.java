package de.rptu.programmierpraktikum2023.p1.util;

import java.io.IOException;

public interface BitInputStream {
    boolean readBit() throws IOException;
    int readByte() throws IOException;
}
