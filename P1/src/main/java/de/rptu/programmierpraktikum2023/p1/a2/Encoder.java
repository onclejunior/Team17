package de.rptu.programmierpraktikum2023.p1.a2;

import de.rptu.programmierpraktikum2023.p1.util.BitOutputStream;

import java.io.IOException;

public interface Encoder {
    void writeData(int data, BitOutputStream out) throws IOException;
}
