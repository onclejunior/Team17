package de.rptu.programmierpraktikum2023.p1.a1;

public class SkewNode<E> {
    Entry<E> entry;
    SkewNode<E> left;
    SkewNode<E> right;

    public SkewNode(Entry<E> entry) {
        this.entry = entry;
        this.left = null;
        this.right = null;
    }
}