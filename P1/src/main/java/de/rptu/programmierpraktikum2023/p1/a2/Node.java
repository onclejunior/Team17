package de.rptu.programmierpraktikum2023.p1.a2;

import java.util.Objects;

public final class Node {
    private final Integer data;
    private final Node left;
    private final Node right;

    /**
     * Constructs a new leaf.
     *
     * @param data the data to store in the leaf
     */
    public Node(int data) {
        this.data = data;
        this.left = null;
        this.right = null;
    }

    /**
     * Constructs a new internal node.
     *
     * @param left  the left subtree (must not be null)
     * @param right the right subtree (must not be null)
     */
    public Node(Node left, Node right) {
        this.data = null;
        this.left = Objects.requireNonNull(left, "The left subtree must not be null!");
        this.right = Objects.requireNonNull(right, "The right subtree must not be null!");
    }

    /**
     * Check whether this node is a leaf or an internal node.
     *
     * @return true if it is a leaf, false if it is an internal node
     */
    public boolean isLeaf() {
        return data != null;
    }

    /**
     * Get the data that is stored in this leaf.
     *
     * @return the stored data
     * @throws UnsupportedOperationException if this node is not a leaf but an internal node
     */
    public int getData() {
        if (data == null) {
            throw new UnsupportedOperationException("An internal node has no data!");
        }
        return data;
    }

    /**
     * Get the left subtree of this internal node.
     *
     * @return the left subtree
     * @throws UnsupportedOperationException if this node is not an internal node but a leaf
     */
    public Node getLeft() {
        if (left == null) {
            throw new UnsupportedOperationException("A leaf node has no left subtree!");
        }
        return left;
    }

    /**
     * Get the right subtree of this internal node.
     *
     * @return the right subtree
     * @throws UnsupportedOperationException if this node is not an internal node but a leaf
     */
    public Node getRight() {
        if (right == null) {
            throw new UnsupportedOperationException("A leaf node has no right subtree!");
        }
        return right;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        Node otherNode = (Node) other;
        return Objects.equals(data, otherNode.data) &&
                Objects.equals(left, otherNode.left) &&
                Objects.equals(right, otherNode.right);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data, left, right);
    }

    @Override
    public String toString() {
        return data == null ? "(" + left + ", " + right + ")" : data.toString();
    }
}
