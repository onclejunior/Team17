package de.rptu.programmierpraktikum2023.p1.util;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

class BitInputStreamImpl implements BitInputStream, Closeable {
    private final InputStream in;
    private final boolean[] buffer = new boolean[8];
    private byte bufferPos = 8;

    public BitInputStreamImpl(InputStream in) {
        this.in = in;
    }

    public boolean readBit() throws IOException {
        if (bufferPos == 8) {
            int x = in.read();
            if (x == -1) {
                throw new IOException("Unexpected end of stream!");
            }
            for (int i = 0; i < 8; i++) {
                buffer[i] = (x & 1 << 7 - i) != 0;
            }
            bufferPos = 0;
        }
        return buffer[bufferPos++];
    }

    public int readByte() throws IOException {
        int x = 0;
        for (int i = 0; i < 8; i++) {
            if (readBit()) {
                x |= 1 << 7 - i;
            }
        }
        return x;
    }

    @Override
    public void close() throws IOException {
        in.close();
    }
}
