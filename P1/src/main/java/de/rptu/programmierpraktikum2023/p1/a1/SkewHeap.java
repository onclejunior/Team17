
package de.rptu.programmierpraktikum2023.p1.a1;

import java.util.NoSuchElementException;
public class SkewHeap<E> implements PriorityQueue<E>{

    SkewNode<E> node;

    //  Wir brauchen size in huffman code
    private int size;

    public SkewHeap() {
        this.node = null;
    }

    private SkewNode<E> merge(SkewNode<E> p , SkewNode<E> q){

        //  falls einer der Heaps leer ist
        if (p == null) return q;
        if (q == null) return p;


        // Sichertellen , dass p eine kleinere Priorität hat
        if (p.entry.priority < q.entry.priority){

            // Swap p->left und p->right
            SkewNode<E> temp = p.right;
            p.right = p.left;
            p.left = temp;

            // Merge q und p->left
            p.left = merge(q,p.left);


            return p;
        }else {
            // Swap q->left und q->right
            SkewNode<E> temp = q.right;
            q.right = q.left;
            q.left = temp;

            // Merge p und q->left
            q.left = merge(p,q.left);

            return q;
        }

    }

    @Override
    public void insert(long priority, E element) {
        // Wir erzeugen einfach ein neuer Node um etwas einzufügen und merge es mit data
        this.node = merge(this.node,new SkewNode<>(new Entry<>(priority,element)));
        this.size++ ;

    }



    @Override
    public Entry<E> extractMin() {

        if (isEmpty()){
            throw new NoSuchElementException();
        }

        // Wir speichern den Eintrag in einem temp und der neue Node wird merge von left und right Node
        Entry<E> temp = node.entry;
        this.node = merge(this.node.left,this.node.right);
        this.size--;

        return temp;
    }


    @Override
    public Entry<E> peek() {

        if (isEmpty()){
            return null;
        }else {
            return node.entry;
        }
    }

    @Override
    public boolean isEmpty() {
        return this.node == null;
    }

    public int size() {
        return this.size;
    }
}