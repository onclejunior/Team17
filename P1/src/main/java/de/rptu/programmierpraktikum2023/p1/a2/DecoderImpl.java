package de.rptu.programmierpraktikum2023.p1.a2;

import de.rptu.programmierpraktikum2023.p1.util.BitInputStream;

import java.io.IOException;

public class DecoderImpl implements Decoder {

    // we store the rootNode in this attribute
    Node rootNode;


    public DecoderImpl(Node rootNode) {
        this.rootNode = rootNode;
    }

    @Override
    public int readData(BitInputStream in) throws IOException {

        // we find and return data
        return findData(in, this.rootNode);
    }

    // helper method for readData to help me find the data
    private int findData(BitInputStream in, Node node) throws IOException {

        // we first check it there is a leaf
        if (node.isLeaf()) {


            // return the data
            return node.getData();

        } else {

            // we check it the bit is false
            if (!in.readBit()) {

                //we move left
                return findData(in, node.getLeft());
            } else {

                // we move right
                return findData(in, node.getRight());
            }
        }
    }
}