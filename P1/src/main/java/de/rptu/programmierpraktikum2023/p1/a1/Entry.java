package de.rptu.programmierpraktikum2023.p1.a1;

import java.util.Objects;

public class Entry<E> {
    public final long priority;
    public final E element;

    public Entry(long priority, E element) {
        this.priority = priority;
        this.element = element;
    }

    @Override
    public String toString() {
        return priority + ": " + element;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        Entry<?> otherEntry = (Entry<?>) other;
        return priority == otherEntry.priority && Objects.equals(element, otherEntry.element);
    }

    @Override
    public int hashCode() {
        return Objects.hash(priority, element);
    }
}
