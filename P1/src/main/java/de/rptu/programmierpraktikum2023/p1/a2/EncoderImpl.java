package de.rptu.programmierpraktikum2023.p1.a2;

import de.rptu.programmierpraktikum2023.p1.util.BitOutputStream;

import java.io.IOException;


public class EncoderImpl implements Encoder {

    // we store the rootNode in this attribute
    Node rootNode;

    //we store the bits of the data in this encoding array
    private boolean[][] encoding;

    // we store the size of the encoded bit in this array
    public int[] bitSize = new int[256];


    // this is to get the size of the Node
    private int lengthOfNode(Node node) {
        if (node == null) {
            return 0;

        } else if (node.isLeaf()) {
            return 1;
        } else {
            return lengthOfNode(node.getLeft()) + lengthOfNode(node.getRight());
        }

    }

    // this helps me to generate the code and store it in the encoding array
    private void codeGenerator(Node node, String code, int bitSize) {

        // we first check it there is a leaf
        if (node.isLeaf()) {

            // we store the code at the row of the data
            for (int i = 0; i < code.length(); i++) {
                //if 0 -> false  , 1 -> true
                this.encoding[node.getData()][i] = code.charAt(i) != '0';

            }


            // we store the length of the bit
            // eg :  "01010" has the size of 5
            this.bitSize[node.getData()] = bitSize;


        } else {


            // we move to the right side
            codeGenerator(node.getRight(), code + "1", bitSize + 1);

            // we move to the left side

            codeGenerator(node.getLeft(), code + "0", bitSize + 1);
        }


    }


    public EncoderImpl(Node rootNode) {
        this.rootNode = rootNode;

        // we use the length of the node to fully create the encoding array
        this.encoding = new boolean[256][lengthOfNode(rootNode) - 1];

        // we generate the code
        codeGenerator(rootNode, "", 0);
    }


    @Override
    public void writeData(int data, BitOutputStream out) throws IOException {


        // we first get the data's bit size and use it has our column
        int cols = this.bitSize[data];


        //writing all the bit into the outputStream
        for (int i = 0; i < cols; i++) {
            out.writeBit(encoding[data][i]);
        }
    }
}
