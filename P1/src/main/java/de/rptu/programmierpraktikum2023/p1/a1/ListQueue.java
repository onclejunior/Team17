package de.rptu.programmierpraktikum2023.p1.a1;

import java.util.LinkedList;
import java.util.NoSuchElementException;

public class ListQueue<E> implements PriorityQueue<E> {

    private LinkedList<Entry<E>> mainQueue;

    // A constructor that creates a new LinkedList.
    public ListQueue() {
        mainQueue = new LinkedList<>();
    }


    @Override
    public void insert(long priority, E element) {
        Entry<E> newQueue = new Entry<>(priority, element);

        if(mainQueue.isEmpty()){
            this.mainQueue.addFirst(newQueue);
        }else{
            int i =0;
            for (Entry<E> entry: mainQueue){
                if(entry.priority <= newQueue.priority){
                    i++;
                    mainQueue.add(i, newQueue);
                } else{
                    mainQueue.add(i,newQueue);
                }
                break;
            }
        }
    }

    @Override
    public Entry<E> extractMin() {
        if(mainQueue.isEmpty()){
            throw new NoSuchElementException();
        }
        return this.mainQueue.removeFirst();
    }

    @Override
    public Entry<E> peek() {
        if (mainQueue.size() == 0) {
            return null;
        } else {
            return mainQueue.getFirst();
        }
    }

    @Override
    public boolean isEmpty() {
        return mainQueue.size() == 0;
    }
}
