package de.rptu.programmierpraktikum2023.p1.a1;

public interface PriorityQueue<E> {
    void insert(long priority, E element);
    Entry<E> extractMin();
    Entry<E> peek();
    boolean isEmpty();
}
