package de.rptu.programmierpraktikum2023.p1.a2;

import de.rptu.programmierpraktikum2023.p1.a1.Entry;
import de.rptu.programmierpraktikum2023.p1.a1.SkewHeap;
import de.rptu.programmierpraktikum2023.p1.util.BitInputStream;
import de.rptu.programmierpraktikum2023.p1.util.BitOutputStream;

import java.io.IOException;


public class HuffmanImpl implements Huffman{
    @Override
    public Node buildTree(long[] frequencies){
        SkewHeap<Node> queue = new SkewHeap<>();
        //creating a leaf and adding it to the queue

        char index = 0;      // element of  entry
        for (long f :frequencies) {
            if (f > 0 ){
                Node leaf = new Node(index);
                queue.insert(f,leaf);
            }
            index++;
        }

        while (queue.size() > 1) {

            // extract 2 entries
            Entry<Node> l = queue.extractMin();
            Entry<Node> r = queue.extractMin();

            // add the priority of those 2 entries
            long newPriority = l.priority + r.priority;

            // insert them back into the queue
            queue.insert(newPriority, new Node(l.element, r.element));
        }

        if (queue.size() > 0) {
            // returning the last node
            return queue.peek().element;
        }else{ return null;}
    }

    @Override
    public Encoder buildEncoder(Node rootNode) {

        return new EncoderImpl(rootNode);

    }

    @Override
    public Decoder buildDecoder(Node rootNode) {
        return new DecoderImpl(rootNode);
    }

    // this helper method converts the Node into a string bit
    private String nodeBit(Node node) {

        //check if it is a leaf or a node
        if (node.isLeaf()) {

            //if leaf we add 1 to the string
            // And also get the data of that leaf ,
            // convert if to bits and add it to the string
            String dataBit = getBit(node.getData());
            return "1" + dataBit;

        } else {

            // if Node we add 0 to the string
            //also get the convert the left and write node to bits
            // and add it to the string
            String left = "0" + nodeBit(node.getLeft());
            String right = nodeBit(node.getRight());


            return left + right;
        }


    }

    // this helper method is to the convert the data into 8 bits
    private String getBit(int n) {
        StringBuilder str = new StringBuilder();


        while (n > 0) {
            if ((n % 2) == 0) {
                //str = "0" + str
                str.insert(0, "0");
            } else {
                //str = "1" + str
                str.insert(0, "1");
            }
            n = n / 2;
        }

        // here after converting the bits and the size is not 1 byte
        // we add 0 to the front of the bits until we get the size of 1 Byte
        if (str.length() < 8) {

            for (int i = str.length(); i < 8; i++) {
                //str = "0" + str
                str.insert(0, "0");
            }
        }
        return str.toString();
    }

    @Override
    public void writeNode(Node node, BitOutputStream out) throws IOException {
        // we get the node bit
        String nodeBit = nodeBit(node);

        // we write the bit
        for (int i = 0; i < nodeBit.length(); i++) {


            out.writeBit(nodeBit.charAt(i) != '0');

        }

    }

    // this helper method helps me read the next 8 bits from the BitInputStream
    // and convert them to string
    private String readNodeBit(BitInputStream in) throws IOException {
        StringBuilder bit = new StringBuilder();
        for (int k = 0; k < 8; k++) {
            if (in.readBit()) {
                //bit += "1"
                bit.append("1");
            } else {
                //bit += "0"
                bit.append("0");
            }
        }
        return bit.toString();
    }

    // this helper method helps me convert string bits to integer
    private int bitToInteger(String bit) {
        int result = 0;
        for (int j = bit.length() - 1; j >= 0; j--) {
            if (bit.charAt(j) == '1') {
                result += Math.pow(2, bit.length() - j - 1);
            }
        }
        return result;
    }


    @Override
    public Node readNode(BitInputStream in) throws IOException {

        // we check if it is a leaf or a Node
        if (in.readBit()) {
            // if it is a leaf we read the next 8 bits
            // convert them to string
            // convert them to integer
            // then create a new leaf with it
            return new Node(bitToInteger(readNodeBit(in)));
        } else {

            // if it is a node
            // we create a node
            // in it, we read the left node and right Node
            return new Node(readNode(in), readNode(in));
        }

    }
}