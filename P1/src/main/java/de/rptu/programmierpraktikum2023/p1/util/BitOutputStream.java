package de.rptu.programmierpraktikum2023.p1.util;

import java.io.IOException;

public interface BitOutputStream {
    void writeBit(boolean bit) throws IOException;
    void writeByte(int data) throws IOException;
}
