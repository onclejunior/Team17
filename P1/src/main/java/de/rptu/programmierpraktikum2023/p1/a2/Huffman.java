package de.rptu.programmierpraktikum2023.p1.a2;

import de.rptu.programmierpraktikum2023.p1.util.BitInputStream;
import de.rptu.programmierpraktikum2023.p1.util.BitOutputStream;

import java.io.IOException;

public interface Huffman {
    Node buildTree(long[] frequencies);
    Encoder buildEncoder(Node rootNode);
    Decoder buildDecoder(Node rootNode);
    void writeNode(Node node, BitOutputStream out) throws IOException;
    Node readNode(BitInputStream in) throws IOException;
}
