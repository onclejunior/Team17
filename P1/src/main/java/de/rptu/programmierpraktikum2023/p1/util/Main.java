package de.rptu.programmierpraktikum2023.p1.util;

import de.rptu.programmierpraktikum2023.p1.a2.Huffman;
import de.rptu.programmierpraktikum2023.p1.a2.HuffmanFactory;

import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        if (args.length != 3) {
            throw new IllegalArgumentException("The program requires 3 parameters!");
        }

        boolean decompress;
        if (args[0].equals("compress")) {
            decompress = false;
        } else if (args[0].equals("decompress")) {
            decompress = true;
        } else {
            throw new IllegalArgumentException("First argument must be 'compress' or 'decompress'!");
        }

        Huffman huffman = HuffmanFactory.getHuffman();

        File inputFile = new File(args[1]);
        if (!inputFile.isFile()) {
            throw new IllegalArgumentException("Input file " + inputFile + " does not exist!");
        }

        File outputFile = new File(args[2]);
        if (outputFile.exists()) {
            throw new IllegalArgumentException("Output file " + outputFile + " already exist!");
        }

        if (decompress) {
            FileOperations.decompressFile(inputFile, outputFile, huffman);
        } else {
            FileOperations.compressFile(inputFile, outputFile, huffman);
        }
    }
}
